import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular, RobotoBold } from '../../bosons/fonts';
import { lightGrey, white } from '../../bosons/colors';

export const GeneralRegulationContainer = styled.View`
  background-color: ${white};
`;

export const RegulationContainer = styled.ScrollView`
  padding-left: ${width(7.5)};
  padding-right: ${width(7.5)};
  width: ${width(100)};
  margin-top: ${width(35.02)};
`;

export const RegulationList = styled.View`
  margin-bottom: ${width(12.08)};
`;

export const RegulationListItem = styled.View`
  margin-top: ${width(6.04)};
`;

export const ListItemText = styled.Text`
  font-size: ${width(4.106)};
  color: ${lightGrey};
`;

export const ListItemTitle = styled(ListItemText)`
  font-family: ${RobotoBold};
`;

export const ListItemDesc = styled(ListItemText)`
  font-family: ${RobotoRegular};
`;
