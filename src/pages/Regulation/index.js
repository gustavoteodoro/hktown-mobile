import React, { PureComponent } from 'react';
import SimpleHeader from '../../molecules/SimpleHeader';
import { pageTitle, regulationItems } from './data.json';

import {
  RegulationContainer, RegulationList, RegulationListItem, ListItemTitle,
  ListItemDesc, GeneralRegulationContainer, ListItemText,
} from './styles';

class Regulation extends PureComponent {
  render() {
    return (
      <GeneralRegulationContainer>
        <SimpleHeader {...this.props} title={pageTitle} />
        <RegulationContainer>
          <RegulationList>
            {regulationItems.map(item => (
              <RegulationListItem key={item.title}>
                <ListItemText>
                  <ListItemTitle>{item.title}</ListItemTitle>
                  <ListItemDesc>{item.description}</ListItemDesc>
                </ListItemText>
              </RegulationListItem>
            ))}
          </RegulationList>
        </RegulationContainer>
      </GeneralRegulationContainer>
    );
  }
}

export default Regulation;
