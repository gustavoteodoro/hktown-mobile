import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/pt-br';
import { width } from 'react-native-dimension';
import SimpleHeader from '../../molecules/SimpleHeader';
import ProfilePic from '../../atoms/ProfilePic';
import { getMyPastEvents } from '../../services/myEvents';
import { postEvaluate } from '../../services/evaluate';
import { HackIcons } from '../../bosons/fonts';
import {
  FeedbackScrollView, FeedbackContainer, FeedbackDateContainer, FeedbackDateTitle,
  FeedbackEvents, FeedbackEventContainer, FeedbackEventTime, FeedbackEventTitle, SpeakerContainer,
  SpeakerImage, SpeakerTextContainer, SpeakerName, SpeakerRole, RateContainer, FeedbackEventDetails,
  Rating, RateTouch, FeedbackEventsNotFound, FeedbackEventsNotFoundText,
} from './styles';
import {
  softBlue, pink,
} from '../../bosons/colors';

moment.locale('pt-br');
class Feedback extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentEvents: [],
      likeLoading: false,
      unlikeLoading: false,
    };
  }

  componentDidMount() {
    this.updateEvents();
  }

  handleEvaluate(eventID, value) {
    const { user } = this.props;
    if (value) {
      this.setState({ likeLoading: true });
    } else {
      this.setState({ unlikeLoading: true });
    }
    postEvaluate(user.token, user.id, eventID, value)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({ likeLoading: false, unlikeLoading: false });
          this.updateEvents();
        }
      });
  }

  updateEvents() {
    const { user } = this.props;
    getMyPastEvents(user.token, user.id)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({ currentEvents: result.result });
        }
      });
  }

  render() {
    const {
      currentEvents,
      likeLoading,
      unlikeLoading,
    } = this.state;
    return (
      <FeedbackContainer>
        <SimpleHeader {...this.props} title="Envie seu feedback" />
        <FeedbackScrollView>
          {currentEvents[0] && (
            <FeedbackDateContainer>
              <FeedbackDateTitle>{moment(currentEvents[0].eventDateStart).format('dddd')} {moment(currentEvents[0].eventDateStart).format('DD/MM')}</FeedbackDateTitle>
            </FeedbackDateContainer>
          )}
          {!currentEvents[0] && (
          <FeedbackEventsNotFound>
            <FeedbackEventsNotFoundText>
              Você não tem eventos aguardando o seu feedback neste momento.
            </FeedbackEventsNotFoundText>
          </FeedbackEventsNotFound>
          )}
          {currentEvents[0] && (
            <FeedbackEvents>
              {currentEvents.map(event => (
                <FeedbackEventContainer key={event.eventID}>
                  <FeedbackEventDetails>
                    <FeedbackEventTime>{moment(event.eventDateStart).format('HH:mm')} — {moment(event.eventDateEnd).format('HH:mm')}</FeedbackEventTime>
                    <FeedbackEventTitle>{event.eventTitle}</FeedbackEventTitle>
                    {(event.eventSpeaker)
                  && (
                    <SpeakerContainer>
                      <SpeakerImage>
                        <ProfilePic
                          uri={event.eventSpeaker.speakerImage}
                          small
                        />
                      </SpeakerImage>
                      <SpeakerTextContainer>
                        <SpeakerName>{event.eventSpeaker.speakerName}</SpeakerName>
                        <SpeakerRole>{event.eventSpeaker.speakerRole}</SpeakerRole>
                      </SpeakerTextContainer>
                    </SpeakerContainer>
                  )}
                  </FeedbackEventDetails>
                  <RateContainer>
                    <RateTouch onPress={() => this.handleEvaluate(event.eventID, false)}>
                      <Rating rotated>
                        {unlikeLoading && (<ActivityIndicator size="large" color={pink} />)}
                        {!unlikeLoading && (<HackIcons name="like" size={width(7.246)} color={pink} />)}
                      </Rating>
                    </RateTouch>
                    <RateTouch onPress={() => this.handleEvaluate(event.eventID, true)}>
                      <Rating>
                        {likeLoading && (<ActivityIndicator size="large" color={softBlue} />)}
                        {!likeLoading && (<HackIcons name="like" size={width(7.246)} color={softBlue} />)}
                      </Rating>
                    </RateTouch>
                  </RateContainer>
                </FeedbackEventContainer>
              ))}
            </FeedbackEvents>
          )}

        </FeedbackScrollView>
      </FeedbackContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(Feedback);
