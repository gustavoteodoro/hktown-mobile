import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoBold, RobotoRegular } from '../../bosons/fonts';
import {
  darkGrey, white, lighestGrey, lightGrey, pink,
} from '../../bosons/colors';

export const FeedbackContainer = styled.View`
  flex: 1;
  flex-direction: column;
  padding-top: 130;
  background-color: ${lighestGrey};
`;

export const FeedbackScrollView = styled.ScrollView``;

export const FeedbackEvents = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
  background-color: ${lighestGrey};
  padding-bottom: 10;
`;

export const FeedbackEventsNotFound = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 200;
`;

export const FeedbackEventsNotFoundText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.35)};
  color: ${darkGrey};
  width: ${width(82)};
  text-align: center;
`;

export const FeedbackDateContainer = styled.View`
  width: ${width(100)};
  background-color: ${lightGrey};
  flex-direction: row;
  align-items: center;
  height: 40;
`;

export const FeedbackDateTitle = styled.Text`
  color: ${white};
  font-family: ${RobotoRegular};
  font-size: ${width(4.831)};
  margin-left: ${width(7.5)};
`;

export const FeedbackEventContainer = styled.View`
  margin-top: 10;
  margin-bottom: 10;
  background-color: ${white};
  width: ${width(81)};
`;

export const FeedbackEventDetails = styled.View`
  padding-top: 20;
  padding-bottom: 20;
  padding-left: 20;
  padding-right: 20;
`;

export const FeedbackEventTime = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(2.9)};
  letter-spacing: 0.2;
  color: ${darkGrey};
`;

export const FeedbackEventTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.35)};
  color: ${pink};
  margin-top: 10;
  margin-bottom: 10;
`;

export const SpeakerContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 10;
  margin-bottom: 10;
`;

export const SpeakerImage = styled.View``;

export const SpeakerTextContainer = styled.View`
  flex-direction: column;
  margin-left: 15;
`;

export const SpeakerName = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(3.865)};
  color: ${darkGrey};
`;

export const SpeakerRole = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(2.9)};
  color: ${darkGrey};
`;

export const RateContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const RateTouch = styled.TouchableWithoutFeedback``;

export const Rating = styled.View`
  width: ${width(40.5)};
  border-color: ${lightGrey};
  border-top-width: 0.5;
  border-left-width: 0.5;
  border-right-width: 0.5;
  border-bottom-width: 0.5;
  height: 60;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  ${props => props.rotated && css`
    transform: rotate(180deg);
  `}
`;
