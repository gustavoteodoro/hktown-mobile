import React, { PureComponent } from 'react';
import SimpleHeader from '../../molecules/SimpleHeader';
import SponsorsFile from '../../assets/sponsors.png';
import { SponsorsScrollView, SponsorsContainer, SponsorsImage } from './styles';

class Sponsors extends PureComponent {
  render() {
    return (
      <SponsorsContainer>
        <SimpleHeader {...this.props} title="Realizadores" />
        <SponsorsScrollView>
          <SponsorsImage source={SponsorsFile} resizeMode="contain" />
        </SponsorsScrollView>
      </SponsorsContainer>
    );
  }
}

export default Sponsors;
