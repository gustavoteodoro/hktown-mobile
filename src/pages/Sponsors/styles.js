import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { softBlue } from '../../bosons/colors';

export const SponsorsContainer = styled.View`
  background-color: ${softBlue};
`;

export const SponsorsScrollView = styled.ScrollView`
  margin-top: ${width(32.75)};
`;

export const SponsorsImage = styled.Image`
  width: ${width(100)};
  height: ${width(100) * 5};
`;
