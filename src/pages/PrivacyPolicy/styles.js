import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular } from '../../bosons/fonts';
import { lightGrey, white } from '../../bosons/colors';

export const PrivacyPolicyScrollView = styled.View`
  background-color: ${white};
`;

export const PrivacyPolicyContainer = styled.ScrollView`
  padding-left: ${width(7.5)};
  padding-right: ${width(7.5)};
  width: ${width(100)};
  margin-top: ${width(41.06)};
`;

export const PrivacyPolicyParagraph = styled.Text`
  font-family: ${RobotoRegular};
  color: ${lightGrey};
  font-size: ${width(4.106)};
  margin-bottom: ${width(6.04)};
`;
