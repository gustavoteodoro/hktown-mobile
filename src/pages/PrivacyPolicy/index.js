import React, { PureComponent } from 'react';
import SimpleHeader from '../../molecules/SimpleHeader';
import {
  PrivacyPolicyContainer, PrivacyPolicyScrollView, PrivacyPolicyParagraph,
} from './styles';
import { pageTitle, paragraphs } from './data.json';

class PrivacyPolicy extends PureComponent {
  render() {
    return (
      <PrivacyPolicyScrollView>
        <SimpleHeader {...this.props} title={pageTitle} />
        <PrivacyPolicyContainer>
          {paragraphs.map(paragraph => (
            <PrivacyPolicyParagraph key={paragraph}>{paragraph}</PrivacyPolicyParagraph>
          ))}
        </PrivacyPolicyContainer>
      </PrivacyPolicyScrollView>
    );
  }
}

export default PrivacyPolicy;
