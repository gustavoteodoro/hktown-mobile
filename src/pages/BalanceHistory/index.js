import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/pt-br';
import SimpleHeader from '../../molecules/SimpleHeader';
import {
  BalanceHistoryContainer,
  HistoryContainerScroll,
  HistoryContainer,
  HistoryItem,
  HistoryItemDateContainer,
  HistoryItemDateBullet,
  HistoryItemDateText,
  HistoryItemContent,
  HistoryItemTitle,
  HistoryItemValue,
} from './styles';

import { historyData } from './data.json';

moment.locale('pt-br');
class BalanceHistory extends PureComponent {
  render() {
    const {
      balanceInfo,
    } = this.props;
    let currentItemDate = '';
    return (
      <BalanceHistoryContainer>
        <SimpleHeader {...this.props} title="Seu saldo é de" balance={balanceInfo.balanceStr} />
        <HistoryContainerScroll>
          <HistoryContainer>
            {balanceInfo.transactions.map((item) => {
              const isNewDate = currentItemDate !== item.time;
              currentItemDate = item.time;
              return (
                <HistoryItem key={item.cashlessCashDescription}>
                  {(isNewDate) && (
                  <HistoryItemDateContainer>
                    <HistoryItemDateBullet />
                    <HistoryItemDateText>
                      {moment(item.time).format('DD/MM')} {moment(item.time).format('dddd')}
                    </HistoryItemDateText>
                  </HistoryItemDateContainer>
                  )}
                  <HistoryItemContent>
                    <HistoryItemTitle recharge={(item.typeName === 'Recarga')}>
                      {(item.typeName === 'Recarga') ? 'Recarga ' : null}
                      {item.cashlessCashDescription}
                    </HistoryItemTitle>
                    <HistoryItemValue recharge={(item.typeName === 'Recarga')}>
                      {item.totalString}
                    </HistoryItemValue>
                  </HistoryItemContent>
                </HistoryItem>
              );
            })}
          </HistoryContainer>
        </HistoryContainerScroll>
      </BalanceHistoryContainer>
    );
  }
}

const mapStateToProps = state => ({
  balanceInfo: state.balance,
});

export default connect(mapStateToProps)(BalanceHistory);
