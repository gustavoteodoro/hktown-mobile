import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { white, softBlue, darkGrey } from '../../bosons/colors';
import { RobotoMedium } from '../../bosons/fonts';

export const BalanceHistoryContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  background: ${white};
`;

export const BalanceHistoryTitle = styled.Text`

`;

export const HistoryContainerScroll = styled.ScrollView`
`;

export const HistoryContainer = styled.View`
  width: ${width(95)};
  background: ${white};
  flex-direction: column;
  border-left-width: 1;
  padding-top: 150;
  border-left-color: #f0f0f0;
  margin-left: ${width(5)};
`;
export const HistoryItem = styled.View`
  border-bottom-width: 1;
  border-bottom-color: #c6c6c6;
  padding-right: 20;
  flex-direction: column;
  margin-left: ${width(5)};
`;

export const HistoryItemDateContainer = styled.View`
  position: relative;
  margin-top: 30;
`;

export const HistoryItemDateBullet = styled.View`
  position: absolute;
  width: 10;
  height: 10;
  background: ${softBlue};
  border-radius: 5;
  left: -${width(6.5)};
`;

export const HistoryItemDateText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  color: ${darkGrey};
  margin-top: -7.5;
`;

export const HistoryItemContent = styled.View`
  padding-top: 20;
  padding-bottom: 20;
  flex-direction: row;
  justify-content: space-between;
`;

export const HistoryItemTitle = styled.Text`
  ${props => props.recharge && css`
    color: ${softBlue};
  `}
`;

export const HistoryItemValue = styled.Text`
  ${props => props.recharge && css`
    color: ${softBlue};
  `}
`;
