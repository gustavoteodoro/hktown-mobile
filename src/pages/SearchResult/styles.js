import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import { white, softBlue } from '../../bosons/colors';

export const SearchResultContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  background: ${white};
`;

export const ResultContent = styled.ScrollView`
  margin-top: 90;
  margin-bottom: 0;
  background: ${white};
  flex: 1;
`;

export const EventsContainer = styled.View`

`;

export const SpeakersContainer = styled.View`

`;

export const LoadContainer = styled.View`
  margin-top: ${width(25)};
`;

export const ResultTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.382)};
  letter-spacing: .2;
  color: ${white};
  background: ${softBlue};
  padding-left: ${width(7)};
  padding-right: ${width(7)};
  padding-top: 10;
  padding-bottom: 10;
  margin-bottom: 10;
`;
