import React, { PureComponent } from 'react';
import SimpleHeader from '../../molecules/SimpleHeader';
import PageLoader from '../../molecules/PageLoader';
import { search } from '../../services/search';
import SpeakerCard from '../../molecules/SpeakerCard';
import EventCard from '../../molecules/EventCard';

import {
  SearchResultContainer, ResultContent, EventsContainer, SpeakersContainer, ResultTitle, SearchResultHeader, LoadContainer,
} from './styles';

class SearchResult extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      events: [],
      speakers: [],
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    const {
      navigation,
    } = this.props;
    search(navigation.state.params.searchSentence)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({
            events: result.result.events,
            speakers: result.result.speakers,
            loading: false,
          });
        }
      });
  }

  render() {
    const {
      navigation,
    } = this.props;

    const {
      loading,
      speakers,
      events,
    } = this.state;
    return (
      <SearchResultContainer>
        <SimpleHeader {...this.props} title={navigation.state.params.searchSentence} oneLine />
        {loading && (<LoadContainer><PageLoader /></LoadContainer>)}
        {!loading && (
        <ResultContent>
          {events[0] && (
            <EventsContainer>
              <ResultTitle>Programação</ResultTitle>
              {events.map(item => (
                <EventCard
                  key={item.eventID}
                  id={item.eventID}
                  title={item.eventTitle}
                  dateStart={item.eventDateStart}
                  dateEnd={item.eventDateEnd}
                  local={item.eventLocal}
                  speaker={item.eventSpeakers[0]}
                  navigation={this.props.navigation}
                  hideAdd
                  hideTime
                  localBottom
                />
              ))}

            </EventsContainer>
          )}
          {speakers[0] && (
            <SpeakersContainer>
              <ResultTitle>Line-up</ResultTitle>
              {speakers.map(item => (
                <SpeakerCard
                  key={item.speakerID.toString()}
                  id={item.speakerID}
                  firstLetter={item.firstLetter}
                  name={item.speakerName}
                  role={item.speakerBio}
                  image={item.speakerImage}
                  tags={item.speakerTags}
                  navigation={navigation}
                />
              ))}
            </SpeakersContainer>
          )
          }

        </ResultContent>
        )}
      </SearchResultContainer>
    );
  }
}

export default SearchResult;
