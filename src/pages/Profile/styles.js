import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { white, darkGrey } from '../../bosons/colors';
import { RobotoBold, RobotoMedium } from '../../bosons/fonts';

export const ProfileContainer = styled.ScrollView`
  background-color: ${white};
`;

export const UserArea = styled.View`
  flex-direction: row;
  align-items: center;
  width: ${width(92.8)};
  margin-left: ${width(7.2)};
  margin-top: 60;
  margin-bottom: 0;
`;

export const UserName = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(5.8)};
  color: ${darkGrey};
  margin-right: 20;
`;

export const PicContainer = styled.View`
  position: absolute;
  right: 20;
`;

export const SocialMediaContainer = styled.View`
  flex-direction: column;
  width: ${width(92.8)};
  margin-left: ${width(7.2)};
  margin-top: 20;
  margin-bottom: 50;
`;

export const SocialMediaText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3)};
  color: ${darkGrey};
  margin-bottom: 20;
`;
