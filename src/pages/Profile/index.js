import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  ProfileContainer, UserName, UserArea, PicContainer, SocialMediaContainer, SocialMediaText,
} from './styles';
import ProfileMenu from '../../organisms/ProfileMenu';
import ProfilePic from '../../atoms/ProfilePic';
import SocialMedias from '../../molecules/SocialMedias';

class Profile extends PureComponent {
  render() {
    const social = {
      speakerFacebook: 'https://www.facebook.com/hacktownfest/',
      speakerInstagram: 'https://www.instagram.com/hacktownsrs/',
      speakerYoutube: 'https://www.youtube.com/channel/UC3OPBgQ1I7tT5WEqElm3i8Q',
    };
    return (
      <ProfileContainer>
        <UserArea>
          <UserName>{this.props.user.name}</UserName>
          {/* <PicContainer>
            <ProfilePic
              uri={this.props.user.avatar.original}
              big
            />
          </PicContainer> */}
        </UserArea>
        <ProfileMenu {...this.props} />
        <SocialMediaContainer>
          <SocialMediaText>Siga-nos pelas Redes Sociais</SocialMediaText>
          <SocialMedias speakerSocial={social} />
        </SocialMediaContainer>
      </ProfileContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(Profile);
