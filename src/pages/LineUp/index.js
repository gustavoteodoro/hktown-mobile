import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Input from '../../atoms/Input';
import SpeakerCard from '../../molecules/SpeakerCard';
import PageLoader from '../../molecules/PageLoader';
import { getAllSpeakers } from '../../services/speakers';
import { setSpeakers } from '../../actions/speakers';

import { LineUpContainer, LineUpContent, LineUpHeader } from './styles';

class LineUp extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      refreshing: false,
      searchText: '',
      speakers: this.props.speakers,
    };
  }

  componentWillMount() {
    const {
      speakers,
    } = this.props;

    if (!(speakers[0])) {
      this.setState({ loading: true });
      this.onUpdateSpeakers();
    } else if (!(speakers[0].speakerBio)) {
      this.setState({ loading: true });
      this.onUpdateSpeakers();
    }
  }

  onUpdateSpeakers() {
    getAllSpeakers()
      .then(res => res.json())
      .then((data) => {
        if (data.success) {
          const finalResult = data.result.map((item, index) => {
            if (index > 0) {
              const currentChar = item.speakerName.slice(0, 1);
              const previousChar = data.result[index - 1].speakerName.slice(0, 1);
              if (currentChar !== previousChar) {
                if ((currentChar !== 'Â') && (previousChar !== 'Â')) {
                  return Object.assign({ firstLetter: true }, item);
                }
                return item;
              }
              return item;
            }
            return Object.assign({ firstLetter: true }, item);
          });
          // eslint-disable-next-line
          this.props.setSpeakers(finalResult);
          this.setState({ loading: false, refreshing: false, speakers: this.props.speakers });
        }
      });
  }

  handleUpdateSpeakers() {
    this.setState({ refreshing: true });
    this.onUpdateSpeakers();
  }

  handleChangeText(e) {
    this.setState({ searchText: e },
      () => {
        const {
          searchText,
        } = this.state;
        const {
          speakers,
        } = this.props;
        if (searchText !== '') {
          const filtredResult = speakers.filter(speaker => speaker.speakerName.match(searchText));
          this.setState({ speakers: filtredResult });
        } else {
          this.setState({ speakers: this.props.speakers });
        }
      });
  }

  render() {
    const {
      navigation,
    } = this.props;

    const {
      loading,
      refreshing,
      speakers,
    } = this.state;

    if (loading) {
      return (<PageLoader />);
    }
    return (
      <LineUpContainer>
        <LineUpHeader>
          <Input
            label="Pesquisar nome"
            search
            onChangeText={e => this.handleChangeText(e)}
          />
        </LineUpHeader>
        {speakers[0]
          && (
          <LineUpContent
            data={speakers}
            keyExtractor={speaker => speaker.speakerID.toString()}
            refreshing={refreshing}
            onRefresh={() => this.handleUpdateSpeakers()}
            renderItem={({ item }) => (
              <SpeakerCard
                key={item.speakerID.toString()}
                id={item.speakerID}
                firstLetter={item.firstLetter}
                name={item.speakerName}
                role={item.speakerBio}
                image={item.speakerImage}
                tags={item.speakerTags}
                navigation={navigation}
              />
            )}
          />
          )
        }
      </LineUpContainer>
    );
  }
}

const mapStateToProps = state => ({
  speakers: state.speakers,
});

const mapDispatchToProps = dispatch => ({
  setSpeakers(speakers) {
    dispatch(setSpeakers(speakers));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LineUp);
