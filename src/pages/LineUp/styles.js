import styled from 'styled-components';
import { Platform } from 'react-native';
import { width } from 'react-native-dimension';
import { white, lightGrey } from '../../bosons/colors';

export const LineUpContainer = styled.View`
  flex: 1;
  flex-direction: column;
  background: ${white};
`;

export const LineUpHeader = styled.View`
  position: absolute;
  width: ${width(100)};
  top: 0;
  z-index:20;
  padding-top: 25;
  background-color: ${white};
  padding-bottom: 0;
  border: 0;
  border-color: ${lightGrey};
  border-bottom-width: ${Platform.OS === 'ios' ? 0 : 1 };
`;

export const LineUpContent = styled.FlatList`
  margin-top: ${width(Platform.OS === 'ios' ? 20 : 18)};
  margin-bottom: 0;
  background: ${white};
`;
