import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoBold, RobotoMedium, RobotoRegular } from '../../bosons/fonts';
import {
  darkGrey, white, lightGrey, lighestGrey, softBlue,
} from '../../bosons/colors';

export const CalendarStatusPreContainer = styled.View`
  background-color: ${lighestGrey};
`;

export const CalendarStatusContainer = styled.View`
  background-color: ${lighestGrey};
  width: ${width(100)};
  height: ${height(100)};
`;

export const CalendarStatusNotFoundTitle = styled.Text`
  margin-top: 200;
  font-family: ${RobotoBold};
  font-size: ${width(4.831)};
  color: ${darkGrey};
  text-align: center;
  margin-left: 30;
  margin-right: 30;
`;

export const CalendarStatusNotFoundDesc = styled.Text`
  margin-top: 20;
  font-family: ${RobotoRegular};
  font-size: ${width(3.865)};
  line-height: ${width(4.831)};
  color: ${darkGrey};
  text-align: center;
  margin-left: 30;
  margin-right: 30;
`;

export const CalendarStatusTitle = styled.Text`
  margin-left: ${width(7.5)};
  width: ${width(85)};
  font-family: ${RobotoBold};
  color: ${darkGrey};
  font-size: ${width(5.8)};
`;

export const LoadContainer = styled.View`
  width: ${width(100)};
  height: ${height(100)};
  justify-content: center;
`;

export const NextEventContainer = styled.View`
  position: absolute;
  width: ${width(100)};
  height: ${height(90)};
  padding-top: 220;
`;

export const NextEventContent = styled.View`
  width: ${width(82)};
  margin-left: ${width(9)};
  flex: none;
  overflow: hidden;
  background: #FBFBFB;
  box-shadow: 0px 3px 6px rgba(37,182,182,.86);
`;

export const CalendarStatusTime = styled.View`
  margin-top: ${width(32)};
  background: ${lightGrey};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: 24;
  padding-right: 24;
  height: 40;
`;
export const CalendarStatusTimeHour = styled.Text`
  color: ${white};
  font-family: ${RobotoBold};
  font-size: ${width(3.865)};
`;
export const CalendarStatusTimeCounter = styled.Text`
  color: ${white};
  font-family: ${RobotoMedium};
  font-size: ${width(3.865)};
  flex-direction: row;
`;

export const ButtonContainer = styled.View`
  
`;

export const OtherOptionsText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(3.865)};
  color: ${softBlue};
  padding-top: 20;
  padding-bottom: 20;
  text-align: center;
`;
