import React, { PureComponent } from 'react';
import { WebBrowser } from 'expo';
import md5 from 'blueimp-md5';
import { connect } from 'react-redux';
import moment from 'moment';
import Swiper from 'react-native-swiper';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { white } from '../../bosons/colors';
import Button from '../../atoms/Button';
import EventCard from '../../molecules/EventCard';
import EventCapacity from '../../atoms/EventCapacity';
import PageLoader from '../../molecules/PageLoader';
import SimpleHeader from '../../molecules/SimpleHeader';
import { getCapacity } from '../../services/capacity';
import { getEventsByRange } from '../../services/events';
import {
  CalendarStatusPreContainer,
  CalendarStatusContainer,
  CalendarStatusNotFoundTitle,
  NextEventContainer,
  NextEventContent,
  LoadContainer,
  CalendarStatusTime,
  CalendarStatusTimeHour,
  CalendarStatusTimeCounter,
  ButtonContainer,
  OtherOptionsText,
} from './styles';

import { eventsNotFoundTitle } from './data.json';

moment.locale('pt-br');
class CalendarStatusOther extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      nextEvents: [],
      currentEvent: 0,
      capictyLoading: false,
    };
  }

  componentWillMount() {
    this.setState({ loading: true });
    const { user, navigation } = this.props;
    const { from, to } = navigation.state.params;
    getEventsByRange(user.token, user.id, moment(from).format('YYYY-MM-DD'), { from: moment(from).format('HH:mm'), to: moment(to).format('HH:mm') }, 3, true)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({ loading: false, capictyLoading: true, nextEvents: result.result });
          result.result.map((event, index) => {
            const md5ID = md5(event.eventID);
            setTimeout(() => {
              getCapacity(md5ID)
                .then(res => res.json())
                .then((cresult) => {
                  if (cresult.success) {
                    if (cresult.success) {
                      this.state.nextEvents[index].count = cresult.ranges[0].count;
                      this.forceUpdate();
                    }
                  }
                })
                .catch(() => {
                  this.state.nextEvents[index].countNotFound = true;
                  this.forceUpdate();
                });
              if ((index + 1) === result.result.length) {
                this.setState({ capictyLoading: false });
              }
            }, index * 1000);
          });
        }
      });
  }

  render() {
    const {
      loading,
      nextEvents,
      currentEvent,
      capictyLoading,
    } = this.state;

    return (
      <CalendarStatusPreContainer>
        <SimpleHeader {...this.props} title="Status da Agenda" solid />
        {loading && (<LoadContainer><PageLoader /></LoadContainer>)}
        {(!loading && nextEvents[currentEvent]) && (
          <CalendarStatusContainer>
            <CalendarStatusTime>
              <CalendarStatusTimeHour>
                {`${moment(nextEvents[currentEvent].eventDateStart).format('HH:mm')} - ${moment(nextEvents[currentEvent].eventDateEnd).format('HH:mm')}`}
              </CalendarStatusTimeHour>
              <CalendarStatusTimeCounter>
                <HackIcons name="time" size={width(3.38)} color={white} /> {moment(nextEvents[currentEvent].eventDateStart).fromNow()}
              </CalendarStatusTimeCounter>
            </CalendarStatusTime>
            <OtherOptionsText>OUTRAS OPÇÕES NO MESMO HORÁRIO</OtherOptionsText>
            <NextEventContainer>
              <Swiper
                loop={false}
                showsButtons={false}
                removeClippedSubviews={false}
                dotStyle={{
                  width: 8,
                  height: 8,
                  borderRadius: 24,
                  backgroundColor: '#B4B4B4',
                  opacity: 0.5,
                }}
                activeDotStyle={{
                  width: 12,
                  height: 12,
                  borderRadius: 24,
                  backgroundColor: '#EF558C',
                }}
                onIndexChanged={index => this.setState({ currentEvent: index })}
              >
                {nextEvents.map(item => (
                  <NextEventContent key={item.eventID}>
                    <EventCard
                      key={item.eventID}
                      id={item.eventID}
                      title={item.eventTitle}
                      dateStart={item.eventDateStart}
                      dateEnd={item.eventDateEnd}
                      local={item.eventLocal}
                      speaker={item.eventSpeakers[0]}
                      navigation={this.props.navigation}
                      hideAdd
                      hideTime
                      localBottom
                    />
                    {capictyLoading && (<EventCapacity loading />)}
                    {!capictyLoading && (
                    <EventCapacity
                      eventCapacity={item.eventCapacity}
                      loading={capictyLoading}
                      count={item.count}
                      countNotFound={item.countNotFound}
                    />
                    )}
                    {item.eventLocalLink && (
                      <ButtonContainer>
                        <Button onClick={() => WebBrowser.openBrowserAsync(item.eventLocalLink)} label="ME LEVE ATÉ LÁ" />
                      </ButtonContainer>
                    )}
                  </NextEventContent>
                ))}
              </Swiper>
            </NextEventContainer>
          </CalendarStatusContainer>
        )}
        {(!loading && !nextEvents[currentEvent]) && (
          <CalendarStatusContainer>
            <CalendarStatusNotFoundTitle>
              {eventsNotFoundTitle}
            </CalendarStatusNotFoundTitle>
          </CalendarStatusContainer>
        )}
      </CalendarStatusPreContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(CalendarStatusOther);
