import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, ActivityIndicator } from 'react-native';

class AuthLoading extends PureComponent {
  componentWillMount() {
    if (this.props.user) {
      if (this.props.user.token && this.props.user.prodVersion) {
        this.props.navigation.navigate('TabNavigator');
      } else {
        this.props.navigation.navigate('AuthNavigator');
      }
    }
  }

  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(AuthLoading);
