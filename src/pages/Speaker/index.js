import React, { PureComponent } from 'react';
import { width } from 'react-native-dimension';
import ProfilePic from '../../atoms/ProfilePic';
import ScrollHeader from '../../molecules/ScrollHeader';
import PageLoader from '../../molecules/PageLoader';
import SpeakerEvent from '../../molecules/SpeakerEvent';
import { getSpeakerById } from '../../services/speakers';
import TopImage1 from '../../assets/speakerheader1.png';
import TopImage2 from '../../assets/speakerheader2.png';
import TopImage3 from '../../assets/speakerheader3.png';
import TopImage4 from '../../assets/speakerheader4.png';
import {
  SpeakerLoaderContainer,
  SpeakerContainer,
  SpeakerContentScroll,
  SpeakerContentContainer,
  SpeakerContent,
  SpeakerTitle,
  SpeakerBio,
  SpeakerName,
  SpeakerTopImage,
  SpeakerProfilePic,
  SpeakerEventsContainer,
  EventTypeTitleContainer,
  EventTypeTitle,
  MediaContainer,
  LoadContainer,
} from './styles';
import { white } from '../../bosons/colors';

class Speaker extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      speaker: {},
      headerMinified: false,
      loading: false,
      topImage: undefined,
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  shuffleArray = arr => arr.sort(() => Math.random() - 0.5);

  componentDidMount() {
    const TopImage = this.shuffleArray([TopImage1, TopImage2, TopImage3, TopImage4])[0];
    this.setState({topImage: TopImage});
  }

  componentWillMount() {
    this.setState({ loading: true });
    const {
      speakerId,
    } = this.props.navigation.state.params;
    getSpeakerById(speakerId)
      .then(res => res.json())
      .then(result => this.setState({ speaker: result.result, loading: false }));
  }

  handleScroll(e) {
    const { headerMinified } = this.state;
    const { y } = e.nativeEvent.contentOffset;
    if ((y > width(47)) && !headerMinified) {
      this.setState({
        headerMinified: true,
      });
    } else if ((y <= width(47)) && headerMinified) {
      this.setState({
        headerMinified: false,
      });
    }
  }

  render() {
    const {
      speaker,
      headerMinified,
      loading,
      topImage
    } = this.state;

    const { navigation } = this.props;
    
    return (
      <SpeakerLoaderContainer>
        {loading && (<LoadContainer><PageLoader /></LoadContainer>)}
        {!loading && (
        <SpeakerContainer>
          <ScrollHeader
            {...this.props}
            color={white}
            minified={headerMinified}
            title={speaker.speakerName}
          />
          <SpeakerContentScroll
            scrollEventThrottle={16}
            onScroll={this.handleScroll}
          >
            <SpeakerProfilePic>
              <ProfilePic
                uri={speaker.speakerImage}
                big
              />
            </SpeakerProfilePic>
            <SpeakerTopImage
              source={topImage}
              resizeMode="cover"
            />
            <SpeakerContentContainer>
              {speaker.speakerName
                && (
                  <SpeakerContent>
                    <SpeakerTitle>
                      <SpeakerName>{speaker.speakerName}</SpeakerName>
                    </SpeakerTitle>
                    <SpeakerBio>{speaker.speakerBio.replace(/<\/?[^>]+(>|$)/g, '')}</SpeakerBio>
                  </SpeakerContent>
                )
              }
              {speaker.spakerTalks[0] && (
              <SpeakerEventsContainer>
                <EventTypeTitleContainer>
                  <EventTypeTitle>{speaker.spakerTalks[1] ? 'Palestras' : 'Palestra'}</EventTypeTitle>
                </EventTypeTitleContainer>
                {speaker.spakerTalks.map(event => (
                  <SpeakerEvent
                    event={event}
                    key={event.eventID}
                    navigation={navigation}
                  />
                ))}
              </SpeakerEventsContainer>
              )
              }
              {speaker.spakerShows[0] && (
              <SpeakerEventsContainer>
                <EventTypeTitleContainer>
                  <EventTypeTitle>{speaker.spakerShows[1] ? 'Shows' : 'Show'}</EventTypeTitle>
                </EventTypeTitleContainer>
                {speaker.spakerShows.map(event => (
                  <SpeakerEvent
                    event={event}
                    key={event.eventID}
                    navigation={navigation}
                  />
                ))}
              </SpeakerEventsContainer>
              )
              }
              {speaker.spakerWorkshops[0] && (
              <SpeakerEventsContainer>
                <EventTypeTitleContainer>
                  <EventTypeTitle>{speaker.spakerWorkshops[1] ? 'Workshops' : 'Workshop'}</EventTypeTitle>
                </EventTypeTitleContainer>
                {speaker.spakerWorkshops.map(event => (
                  <SpeakerEvent
                    event={event}
                    key={event.eventID}
                    navigation={navigation}
                  />
                ))}
              </SpeakerEventsContainer>
              )
              }
            </SpeakerContentContainer>
          </SpeakerContentScroll>
        </SpeakerContainer>
        )}

      </SpeakerLoaderContainer>
    );
  }
}

export default Speaker;
