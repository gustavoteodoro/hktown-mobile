import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoRegular, RobotoMedium, RobotoBold } from '../../bosons/fonts';
import {
  darkGrey, white, pink,
} from '../../bosons/colors';

export const SpeakerLoaderContainer = styled.View`
  background-color: ${white};
  height: ${height(100)};
`;

export const SpeakerContainer = styled.View`
  height: ${height(100)};
`;

export const SpeakerContentScroll = styled.ScrollView`
  flex: 1;
  flex-direction: column;
`;

export const SpeakerContentContainer = styled.View`
  background-color: ${white};
  padding-top: ${height(1.2)};
  padding-bottom: 100;
`;

export const SpeakerProfilePic = styled.View`
  position: absolute;
  z-index: 1;
  top: 218;
  left: ${width(7.7)};
`;

export const SpeakerTopImage = styled.Image`
  width: 100%;
  height: 256;
  z-index: -1;
`;

export const SpeakerContent = styled.View`
  margin-top: 0;
  margin-left: ${width(7.7)};
  width: ${width(84.6)};
  z-index: 20;
`;

export const SpeakerTitle = styled.View`
  flex-direction: row;
  align-items: flex-end;
`;

export const SpeakerName = styled.Text`
  font-family: ${RobotoBold};
  color: ${darkGrey};
  font-size: ${width(4.35)};
  margin-left: ${width(25)};
  padding-bottom: 12;
`;

export const SpeakerBio = styled.Text`
  font-family: ${RobotoRegular};
  color: ${darkGrey};
  font-size: ${width(3.382)};
  line-height: ${width(4.831)};
  margin-top: 25;
`;

export const SpeakerEventsContainer = styled.View`
  margin-left: auto;
  margin-right: auto;
  width: ${width(84.6)};
`;

export const EventTypeTitleContainer = styled.View`
  border-bottom-color: #c6c6c6;
  border-bottom-width: 1;
  padding-bottom: 20;
  padding-top: 30;
  margin-bottom: 0;
`;

export const EventTypeTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  line-height: ${width(5.8)};
  color: ${pink};
  width: ${width(90)};
`;

export const MediaContainer = styled.View`
  margin-left: ${width(7.7)};
  width: ${width(92.3)};
  margin-top: 0;
`;

export const LoadContainer = styled.View`
  margin-top: ${width(25)};
`;
