import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/pt-br';
import EventCard from '../../molecules/EventCard';
import PageLoader from '../../molecules/PageLoader';
import MyCalendarHeader from '../../organisms/MyCalendarHeader';
import Toaster from '../../organisms/Toaster';
import { getMyEvents, addEvent, removeEvent } from '../../services/myEvents';
import { getEventsByRange } from '../../services/events';
import EventTime from '../../atoms/EventTime';
import AddEvent from '../../atoms/AddEvent';
import { setMyEvents } from '../../actions/myEvents';

import {
  MyCalendarContainer,
  EventList,
  MyEventCard,
  ToasterEvents,
  ToaesterEventsTitle,
  ToasterEventsContent,
  ToasterEventsNotFoundContent,
  ToasterEventsNotFoundText,
  EventCardContainer,
  ToasterLoader,
  MyCalendarLoader,
} from './styles';

import { dates } from './data.json';

moment.locale('pt-br');
class MyCalendar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      toaster: false,
      toasterEvents: [],
      toasterLoading: false,
      refreshing: false,
      currentDate: '2018-09-06',
      toasterItem: {},
      loading: false,
    };
  }

  componentDidMount() {
    const { myEvents } = this.props;
    const { currentDate } = this.state;
    if (!myEvents[currentDate]) {
      this.setState({ loading: true });
      this.updateEvents(currentDate);
    }
  }

  handleChangeDate(date) {
    const { myEvents } = this.props;
    this.setState({ currentDate: date });
    if (!myEvents[date]) {
      this.updateEvents(date);
      this.setState({ loading: true });
    }
  }

  updateEvents(currentDate, refresh) {
    const { user } = this.props;
    if (refresh) {
      this.setState({ refreshing: true });
    }
    getMyEvents(user.token, user.id, currentDate)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setMyEvents({ [currentDate]: result.result });
        }
        this.setState({ refreshing: false, loading: false });
      });
  }

  handleToaster(item) {
    const { currentDate } = this.state;
    const { user } = this.props;
    this.setState({ toasterLoading: true, toaster: true, toasterItem: item });
    getEventsByRange(user.token, user.id, currentDate, { from: item.from, to: item.to })
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({ toasterLoading: false, toasterEvents: result.result.filter(event => !event.isFavorite) });
        }
      });
  }

  handleAddEvent(eventId) {
    const { user } = this.props;
    const { currentDate } = this.state;
    this.setState({ toasterLoading: true });
    addEvent(user.token, user.id, eventId)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.setState({ toasterLoading: false, toaster: false });
          this.updateEvents(currentDate, true);
        }
      });
  }

  handleRemoveEvent(eventId) {
    const { user } = this.props;
    const { currentDate } = this.state;
    removeEvent(user.token, user.id, eventId)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.updateEvents(currentDate);
        }
      });
  }

  render() {
    const {
      toaster,
      toasterItem,
      toasterEvents,
      toasterLoading,
      currentDate,
      refreshing,
      loading,
    } = this.state;

    const {
      myEvents,
    } = this.props;

    const currentEvents = myEvents[currentDate];

    return (
      <MyCalendarContainer>
        {toaster && (
          <Toaster
            hideToaster={() => this.setState({
              toaster: false,
              toasterLoading: false,
            })}
            component={(
              <ToasterEvents>
                <ToaesterEventsTitle>{`${moment(currentDate).format('dddd')} ${moment(currentDate).format('DD/MM')}`}</ToaesterEventsTitle>
                <EventTime
                  initHour={toasterItem.from}
                  endHour={toasterItem.to}
                />
                {toasterLoading && (
                  <ToasterLoader>
                    <PageLoader />
                  </ToasterLoader>
                )}
                {(!toasterLoading && toasterEvents[0]) && (
                  <ToasterEventsContent>
                    {toasterEvents.map(event => (
                      <EventCard
                        key={event.eventID}
                        id={event.eventID}
                        title={event.eventTitle}
                        dateStart={event.eventDateStart}
                        dateEnd={event.eventDateEnd}
                        local={event.eventLocal}
                        speaker={event.eventSpeakers[0]}
                        navigation={this.props.navigation}
                        hideTime
                        hideAdd
                        localBottom
                        shortCutAdd
                        onAddEvent={() => this.handleAddEvent(event.eventID)}
                      />
                    ))}
                  </ToasterEventsContent>
                )}
                {(!toasterLoading && !toasterEvents[0]) && (
                  <ToasterEventsNotFoundContent>
                    <ToasterEventsNotFoundText>
                      Você já adiconou todos os eventos deste horário aos seus favoritos.
                    </ToasterEventsNotFoundText>
                  </ToasterEventsNotFoundContent>
                )}
              </ToasterEvents>
            )}
          />
        )}
        <MyCalendarHeader
          dates={dates}
          currentDate={currentDate}
          onChangeDate={date => this.handleChangeDate(date)}
        />
        {loading && (
        <MyCalendarLoader>
          <PageLoader />
        </MyCalendarLoader>
        )}
        {(!loading && currentEvents)
          && (
            <EventList
              data={currentEvents}
              keyExtractor={item => item.from}
              refreshing={refreshing}
              onRefresh={() => this.updateEvents(currentDate, true)}
              renderItem={({ item }) => (
                <MyEventCard>
                  <EventTime
                    initHour={item.from}
                    endHour={item.to}
                  />
                  {item.events[0] && (
                    <EventCardContainer>
                      {item.events.map(event => (
                        <EventCard
                          key={event.eventID}
                          id={event.eventID}
                          title={event.eventTitle}
                          dateStart={event.eventDateStart}
                          dateEnd={event.eventDateEnd}
                          local={event.eventLocal}
                          speaker={event.eventSpeakers[0]}
                          onEventAdded={() => this.handleAddEvent(event)}
                          onRemoveEvent={() => this.handleRemoveEvent(event.eventID)}
                          navigation={this.props.navigation}
                          hideAdd
                          shortCutRemove
                          localBottom
                          hideTime
                        />
                      ))
                    }
                    </EventCardContainer>
                  )}
                  <AddEvent
                    label="ADICIONAR EVENTOS"
                    onEventAdded={() => this.handleToaster(item)}
                  />
                </MyEventCard>

              )}
            />
          )
        }
      </MyCalendarContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  myEvents: state.myEvents,
});

const mapDispatchToProps = dispatch => ({
  setMyEvents(events) {
    dispatch(setMyEvents(events));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCalendar);
