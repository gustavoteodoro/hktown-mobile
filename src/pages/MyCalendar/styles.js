import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoBold, RobotoMedium } from '../../bosons/fonts';
import { white, softBlue } from '../../bosons/colors';

export const MyCalendarContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  background: #fff;
`;


export const EventList = styled.FlatList`
  margin-top: ${width(22)};
`;

export const ToasterEvents = styled.View`

`;

export const ToaesterEventsTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.831)};
  background: ${softBlue};
  color: ${white};
  padding-left: 25;
  padding-top: 20;
  padding-bottom: 10;
`;

export const ToasterEventsContent = styled.ScrollView`
  height: ${height(40)}
`;

export const EventCardContainer = styled.View`

`;

export const MyEventCard = styled.View`

`;

export const MyCalendarLoader = styled.View`
  height: ${height(100)};
`;

export const ToasterLoader = styled.View`
  height: 200;
`;

export const ToasterEventsNotFoundContent = styled.View`

`;

export const ToasterEventsNotFoundText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4)};
  color: ${softBlue};
  margin-top: 20;
  padding-left: 25;
  padding-right: 25;
  margin-bottom: 20;
  text-align: center;
`;
