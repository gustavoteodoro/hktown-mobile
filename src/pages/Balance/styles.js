import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoBold, RobotoRegular } from '../../bosons/fonts';
import {
  darkGrey, white, softBlue, pink, lightGrey,
} from '../../bosons/colors';

export const BalanceScrollView = styled.ScrollView`
  background-color: ${white};
  margin-top: ${width(32)};
`;

export const BalanceContainer = styled.View`
`;

export const BalanceTitle = styled.Text`
  margin-left: ${width(7.5)};
  width: ${width(85)};
  font-family: ${RobotoBold};
  color: ${darkGrey};
  font-size: ${width(5.8)};
  margin-bottom: 10;
`;

export const BalanceValueContainer = styled.View`
  flex-direction: column;
  align-items: center;
  background-color: ${darkGrey};
  width: ${width(100)};
  padding-top: 50;
  padding-bottom: 50;
`;

export const BalanceSmallText = styled.Text`
  font-family: ${RobotoRegular};
  color: ${white};
  letter-spacing: 0.2;
  font-size: ${width(2.9)};
  margin-top: 20;
  margin-bottom: 5;
`;

export const BalanceMediumText = styled.Text`
  font-family: ${RobotoBold};
  color: ${white};
  letter-spacing: 0.2;
  font-size: ${width(6.28)};
`;

export const ViewBalanceTouch = styled.TouchableWithoutFeedback`

`;

export const ViewBalanceContainer = styled.View`
  width: ${width(100)};
  background-color: ${softBlue};
  height: 50;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const ViewBalanceText = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.382)};
  color: ${white};
  letter-spacing: 0.2;
  margin-right: 15;
`;

export const RechargePlacesContainer = styled.View`
  margin-left: ${width(7.5)};
  width: ${width(92.5)};
  flex-direction: column;
  margin-bottom: 50;
`;

export const RechargePlaceTitleContainer = styled.View`
  flex-direction: row;
  align-items: center;
  padding-top: 20;
  padding-right: 20;
  padding-bottom: 10;
  padding-left: 0;
  border-bottom-color: ${lightGrey};
  border-bottom-width: 1;
  height: 75;
`;

export const RechargePlaceTitle = styled.Text`
  font-family: ${RobotoBold};
  color: ${pink};
  font-size: ${width(4.831)};
`;
