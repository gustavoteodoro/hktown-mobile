import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import SimpleHeader from '../../molecules/SimpleHeader';
import RechargePlace from '../../atoms/RechargePlace';
import { getBalance } from '../../services/balance';
import { setBalance } from '../../actions/balance';
import {
  BalanceScrollView, BalanceContainer, BalanceValueContainer, BalanceSmallText,
  BalanceMediumText, ViewBalanceTouch, ViewBalanceContainer, ViewBalanceText,
  RechargePlacesContainer, RechargePlaceTitle, RechargePlaceTitleContainer,
} from './styles';
import { white } from '../../bosons/colors';
import {
  pageTitle, descText, buttonText, placesTitle, rechargePlaces,
} from './data.json';

class Balance extends PureComponent {
  componentWillMount() {
    const { user } = this.props;
    getBalance(user.cpf)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setBalance({
            balanceStr: result.personInfo.balanceStr,
            transactions: result.personInfo.transactions,
          });
        }
      });
  }

  render() {
    const {
      balanceInfo,
    } = this.props;

    return (
      <BalanceContainer>
        <SimpleHeader {...this.props} title={pageTitle} />
        <BalanceScrollView>
          <BalanceValueContainer>
            <HackIcons name="band" size={width(19.32)} color={white} />
            <BalanceSmallText>{descText}</BalanceSmallText>
            {balanceInfo.balanceStr
              && (<BalanceMediumText>{balanceInfo.balanceStr}</BalanceMediumText>)
            }
          </BalanceValueContainer>
          <ViewBalanceTouch onPress={() => this.props.navigation.navigate('BalanceHistory')}>
            <ViewBalanceContainer>
              <ViewBalanceText>{buttonText.toUpperCase()}</ViewBalanceText>
              <HackIcons name="arrow" size={width(3.38)} color={white} />
            </ViewBalanceContainer>
          </ViewBalanceTouch>
          <RechargePlacesContainer>
            <RechargePlaceTitleContainer>
              <RechargePlaceTitle>{placesTitle}</RechargePlaceTitle>
            </RechargePlaceTitleContainer>
            {rechargePlaces.map(place => (
              <RechargePlace
                key={place.name}
                title={place.name}
                address={place.address}
                link={place.link}
              />
            ))}
          </RechargePlacesContainer>
        </BalanceScrollView>
      </BalanceContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  balanceInfo: state.balance,
});

const mapDispatchToProps = dispatch => ({
  setBalance(balance) {
    dispatch(setBalance(balance));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Balance);
