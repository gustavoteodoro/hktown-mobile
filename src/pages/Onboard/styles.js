import styled from 'styled-components';
import { width, height } from 'react-native-dimension';

export const OnboardContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-start;
`;

export const OnboardLogo = styled.Image`
  width: ${width(70)};
  margin-top: ${width(2.9)};
`;

export const OnboardFeatureContainer = styled.View`
  width: ${width(82)};
  height: ${height(50.1)};
`;

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: ${width(9)};
  left: ${width(9)};
  margin-top: 0;
`;
