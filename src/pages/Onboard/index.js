import React, { PureComponent } from 'react';
import Swiper from 'react-native-swiper';
import Button from '../../atoms/Button';
import LogoHacktown from '../../assets/logo-hacktown.png';
import OnboardFeature from '../../molecules/OnboardFeature';

import {
  OnboardContainer,
  OnboardLogo,
  OnboardFeatureContainer,
  ButtonContainer,
} from './styles';

import { features } from './data.json';

class Onboard extends PureComponent {
  render() {
    const {
      navigation,
    } = this.props;

    return (
      <OnboardContainer>
        <OnboardLogo
          resizeMode="contain"
          source={LogoHacktown}
        />
        <OnboardFeatureContainer>
          <Swiper
            loop={false}
            showsButtons={false}
            paginationStyle={{ bottom: -40 }}
            dotStyle={{
              width: 6,
              height: 6,
              borderRadius: 6,
              backgroundColor: '#25B6B6',
              opacity: 0.5,
            }}
            activeDotStyle={{
              width: 9,
              height: 9,
              borderRadius: 9,
              backgroundColor: '#25B6B6',
            }}
          >
            {features.map(feature => (
              <OnboardFeature
                key={feature.id}
                id={feature.id}
                icon={feature.icon}
                name={feature.name}
                description={feature.description}
              />
            ))}
          </Swiper>
        </OnboardFeatureContainer>
        <ButtonContainer>
          <Button onClick={() => navigation.navigate('Login')} label="ENTRAR" />
        </ButtonContainer>
      </OnboardContainer>
    );
  }
}

export default Onboard;
