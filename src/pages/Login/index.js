import React, { PureComponent } from 'react';
import { Permissions, Notifications } from 'expo';
import { TouchableWithoutFeedback } from 'react-native';
import md5 from 'blueimp-md5';
import { connect } from 'react-redux';
import { TextInputMask } from 'react-native-masked-text';
import SimpleHeader from '../../molecules/SimpleHeader';
import { InputField } from '../../atoms/Input/styles';
import Button from '../../atoms/Button';
import { register } from '../../services/register';
import { getBalance } from '../../services/balance';
import { clearEvents } from '../../actions/events';
import { clearMyEvents } from '../../actions/myEvents';
import { clearSpeakers } from '../../actions/speakers';
import { setUser, clearUser } from '../../actions/user';

import {
  LoginContainer,
  LoginFormContainer,
  LoginTerms,
  LoginTermsContent,
  LoginTermsText,
  LoginTermsLink,
  LoginForm, FormError,
  ErrorWrapper,
} from './styles';

let pushToken;
async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS,
  );
  let finalStatus = existingStatus;
  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  if (finalStatus !== 'granted') {
    return;
  }

  pushToken = await Notifications.getExpoPushTokenAsync();
}

class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cpf: '',
      loading: false,
      focused: false,
    };
  }

  componentDidMount() {
    this.props.clearUser();
    this.props.clearEvents();
    this.props.clearMyEvents();
    this.props.clearSpeakers();
    registerForPushNotificationsAsync();
  }

  handleSubmit() {
    const {
      cpf,
      loading,
    } = this.state;

    if (!loading) {
      const pureCpf = cpf.replace(/[^\d]+/g, '');
      const finalCpf = `${pureCpf.slice(0, 3)}.${pureCpf.slice(3, 6)}.${pureCpf.slice(6, 9)}-${pureCpf.slice(9, 11)}`;
      if (pureCpf.length === 11) {
        getBalance(finalCpf)
          .then(res => res.json())
          .then((result) => {
            if (result.success) {
              if (result.exists) {
                this.setState({ loading: true });
                const userID = md5(Date(), cpf, false);
                const key = '20jf1kJ#mfs@JFMslk;o3p09elk.sadp9813hsd,zD';
                this.props.setUser({
                  name: result.personInfo.name, cpf: result.personInfo.cpf, id: userID, pushToken,
                });
                const hash = md5(userID, key, false);
                register(userID, cpf, hash, pushToken)
                  .then(res => res.json())
                  .then((regResult) => {
                    if (regResult.success) {
                      this.props.setUser({ token: regResult.token, notifications: true, prodVersion: true });
                      this.props.navigation.navigate('TabNavigator');
                    }
                  });
              } else {
                this.setState({ formError: 'CPF não encontrado. Por favor, procure a equipe de credenciamento no evento para te auxiliar.' });
              }
            }
          });
      } else {
        this.setState({ formError: 'Por favor insira seu CPF e tente novamente.' });
      }
    }
  }

  render() {
    const {
      cpf,
      loading,
      focused,
      formError,
    } = this.state;
    return (
      <LoginContainer focused={focused}>
        <SimpleHeader {...this.props} title="Entrar" solid oneLine={!!focused} />
        <LoginFormContainer focused={focused}>
          <LoginForm focused={focused}>
            <TextInputMask
              refInput={ref => this.myDateText = ref}
              type="cpf"
              placeholder="CPF"
              keyboardType="numeric"
              value={cpf}
              onChangeText={e => this.setState({ cpf: e.toLocaleLowerCase() })}
              onFocus={() => this.setState({ focused: true })}
              onBlur={() => this.setState({ focused: false })}
              solid
              maxLength={14}
              underlineColorAndroid="transparent"
              customTextInput={InputField}
            />
          </LoginForm>
          <ErrorWrapper>
            {formError && (
              <FormError focused={focused}>{formError}</FormError>
            )}
          </ErrorWrapper>
        </LoginFormContainer>
        <LoginTerms focused={focused}>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Regulation')}>
            <LoginTermsContent>
              <LoginTermsText>Ao digitar seu CPF e clicar em "Entrar", você concorda com o <LoginTermsLink>Regulamento</LoginTermsLink> do Hack Town 2018.</LoginTermsText>
            </LoginTermsContent>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('PrivacyPolicy')}>
            <LoginTermsContent>
              <LoginTermsText>Lidaremos com as informações sobre você de acordo com a nossa <LoginTermsLink>Política de Privacidade</LoginTermsLink>.</LoginTermsText>
            </LoginTermsContent>
          </TouchableWithoutFeedback>
        </LoginTerms>
        <Button onClick={() => this.handleSubmit()} label="ENTRAR" loading={loading} />
      </LoginContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setUser(user) {
    dispatch(setUser(user));
  },
  clearUser() {
    dispatch(clearUser());
  },
  clearEvents() {
    dispatch(clearEvents());
  },
  clearMyEvents() {
    dispatch(clearMyEvents());
  },
  clearSpeakers() {
    dispatch(clearSpeakers());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
