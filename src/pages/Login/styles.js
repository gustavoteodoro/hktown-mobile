import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoRegular, RobotoBold } from '../../bosons/fonts';
import { white, darkGrey, pink } from '../../bosons/colors';

export const LoginContainer = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  background: ${white};
  padding-bottom: ${height(5)};
  ${props => props.focused && css`
    padding-top: ${height(17)};
    justify-content: flex-start;
  `}
`;

export const LoginTerms = styled.View`
  margin-left: ${width(9)};
  margin-right: ${width(9)};
  margin-bottom: ${width(13.53)};
  flex-wrap: wrap;
  align-items: flex-start;
  flex-direction: row;
  ${props => props.focused && css`
    margin-bottom: ${width(5)};
  `}
`;

export const LoginTermsContent = styled.View`
  margin-bottom: ${width(2)};
`;

export const LoginTermsText = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.38)};
  line-height: ${width(5.78)};
  color: ${darkGrey};
`;

export const LoginTermsLink = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.38)};
  line-height: ${width(5.78)};
  color: ${darkGrey};
  text-decoration: underline;
`;

export const LoginForm = styled.View`
  width: ${width(82)};
`;

export const LoginFormContainer = styled.View`
`;

export const FormError = styled.Text`
  width: ${width(82)};
  font-family: ${RobotoBold};
  font-size: ${width(3.38)};
  line-height: ${width(3.38)};
  color: ${pink};
  text-align: left;
  padding-bottom: 10;
`;

export const ErrorWrapper = styled.View`
  margin-top: ${width(4)};
`;
