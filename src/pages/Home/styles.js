import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import {
  white, softBlue, darkGrey, lightGrey, pink,
} from '../../bosons/colors';

export const HomeContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const HomeInputContainer = styled.View`
  position: absolute;
  top: 0;
  margin-top: 80;
  left: ${width(9)};
  z-index: 20;
  width: ${width(82)};
  ${props => props.searchFocused && css`
    left: 0;
    width: ${width(100)};
    border-bottom-width: 1;
    border-bottom-color: ${lightGrey};
  `}
`;

export const HomeContent = styled.View`
  flex: 1;
  z-index: 10;
`;


export const HomeSearchContainer = styled.View`
  position: absolute;
  display: flex;
  top: 0;
  left: -${width(100)};
  z-index: 10;
  width: ${width(100)};
  height: ${height(100)};
  background: ${white};
  opacity: 1;
  ${props => props.searchFocused && css`
    display: flex;
    opacity: 1;
    left: 0;
  `}
`;

export const HomeSearchSuggestionsContainer = styled.ScrollView`
  margin-top: ${width(37)};
  margin-bottom: ${width(17)};
`;

export const HomeSearchSuggestionThemeTouch = styled.TouchableWithoutFeedback`
`;

export const HomeSearchSuggestion = styled.View`
`;

export const HomeSearchSuggestionTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.382)};
  letter-spacing: .2;
  color: ${white};
  background: ${softBlue};
  padding-left: ${width(7)};
  padding-right: ${width(7)};
  padding-top: 10;
  padding-bottom: 10;
  margin-bottom: 10;
`;

export const HomeSearchSuggestionTheme = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(5.314)};
  color: ${darkGrey};
  padding-left: ${width(7)};
  padding-right: ${width(7)};
  margin-top: 10;
  margin-bottom: 10;
`;

export const HomeSearchClose = styled.View`
  position: absolute;
  display: none;
  opacity: 0;
  top: 35;
  left: 20;
  z-index: 30;
  ${props => props.searchFocused && css`
    display: flex;
    opacity: 1;
  `}
`;

export const HomeSearchDescription = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.63)};
  color: ${darkGrey};
  padding-left: ${width(7)};
  padding-top: 40;
  padding-bottom: 20;
`;

export const HomeCloseTouch = styled.TouchableWithoutFeedback`

`;

export const HomeContentLoading = styled.View`
  width: ${width(100)};
  height: ${height(100)};
  justify-content: center;
  align-items: center;
  background: ${pink};
`;
