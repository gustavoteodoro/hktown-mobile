import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Keyboard } from 'react-native';
import Swiper from 'react-native-swiper';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { darkGrey } from '../../bosons/colors';
import PageLoader from '../../molecules/PageLoader';
import Input from '../../atoms/Input';
import HomeFeature from '../../molecules/HomeFeature';
import { getHighlights } from '../../services/highlights';
import { setHighlights } from '../../actions/highlights';

import {
  HomeContainer,
  HomeContent,
  HomeInputContainer,
  HomeSearchContainer,
  HomeSearchSuggestionThemeTouch,
  HomeSearchSuggestion,
  HomeSearchSuggestionsContainer,
  HomeSearchSuggestionTitle,
  HomeSearchSuggestionTheme,
  HomeSearchClose,
  HomeSearchDescription,
  HomeCloseTouch,
  HomeContentLoading,
} from './styles';

import { features, suggestions } from './data.json';

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchSentence: '',
      searchFocused: false,
      loading: false,
    };
  }

  componentWillMount() {
    if (!this.props.highlights[0]) {
      this.setState({ loading: true });
    }
    getHighlights()
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setHighlights(result.result);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
        }
      });
  }

  handleSearchSubmit() {
    const {
      searchSentence,
    } = this.state;
    if (searchSentence !== '') {
      this.props.navigation.navigate('SearchResult', { searchSentence });
      this.setState({ searchSentence: '', searchFocused: false });
    }
  }

  handleClickSuggestion(searchSentence) {
    this.props.navigation.navigate('SearchResult', { searchSentence });
    this.setState({ searchSentence: '', searchFocused: false });
  }

  render() {
    const {
      searchSentence,
      searchFocused,
      loading,
    } = this.state;

    const {
      highlights,
    } = this.props;

    return (
      <HomeContainer>
        {loading && (
          <HomeContentLoading>
            <PageLoader />
          </HomeContentLoading>
        )}
        {(!loading && !highlights[0]) && (
          <HomeContentLoading />
        )}
        {(!loading && highlights[0]) && (
        <HomeContent>
          <Swiper
            loop={false}
            showsButtons={false}
            dotStyle={{
              width: 6,
              height: 6,
              borderRadius: 6,
              backgroundColor: '#fff',
              opacity: 0.5,
            }}
            activeDotStyle={{
              width: 9,
              height: 9,
              borderRadius: 9,
              backgroundColor: '#fff',
            }}
          >
            {highlights.map((item, i) => (
              <HomeFeature
                key={item.title}
                id={i}
                name={item.title}
                description={item.text}
              />
            ))}
          </Swiper>
        </HomeContent>
        )}
        <HomeInputContainer
          searchFocused={searchFocused}
        >
          <Input
            label="O que você curte?"
            search
            onFocus={() => this.setState({ searchFocused: true })}
            searchFocused={searchFocused}
            value={searchSentence}
            onSubmitEditing={() => this.handleSearchSubmit()}
            returnKeyType="search"
            onChangeText={e => this.setState({ searchSentence: e })}
          />
        </HomeInputContainer>
        <HomeCloseTouch
          onPress={() => {
            this.setState({ searchFocused: false });
            Keyboard.dismiss();
          }}
          hitSlop={{
            top: 20,
            left: 20,
            bottom: 20,
            right: 20,
          }}
        >
          <HomeSearchClose searchFocused={searchFocused}>
            <HackIcons name="close" size={width(4.831)} color={darkGrey} />
          </HomeSearchClose>
        </HomeCloseTouch>
        <HomeSearchContainer searchFocused={searchFocused}>
          {(searchSentence === '')
            && (
              <HomeSearchSuggestionsContainer onScroll={() => Keyboard.dismiss()}>
                <HomeSearchDescription>
                  Escolha um dos temas a seguir
                </HomeSearchDescription>
                {suggestions.map(suggestion => (
                  <HomeSearchSuggestion
                    key={suggestion.title}
                  >
                    <HomeSearchSuggestionTitle>
                      {suggestion.title}
                    </HomeSearchSuggestionTitle>
                    {suggestion.themes.map(theme => (
                      <HomeSearchSuggestionThemeTouch
                        onPress={() => this.handleClickSuggestion(theme)}
                        key={theme}
                      >
                        <HomeSearchSuggestionTheme>
                          {theme}
                        </HomeSearchSuggestionTheme>
                      </HomeSearchSuggestionThemeTouch>
                    ))
                  }
                  </HomeSearchSuggestion>
                ))
            }
              </HomeSearchSuggestionsContainer>
            )
          }
        </HomeSearchContainer>
      </HomeContainer>
    );
  }
}

const mapStateToProps = state => ({
  highlights: state.highlights,
});

const mapDispatchToProps = dispatch => ({
  setHighlights(highlights) {
    dispatch(setHighlights(highlights));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
