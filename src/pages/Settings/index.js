import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { setUser } from '../../actions/user';
import ProfileMenuItem from '../../atoms/ProfileMenuItem';
import SimpleHeader from '../../molecules/SimpleHeader';
import { setNotifications } from '../../services/notifications';
import { SettingsContainer, MenuContainer } from './styles';
import { items } from './data.json';

class Settings extends PureComponent {
  handleNotifications() {
    const { user } = this.props;
    if (user.notifications) {
      this.props.setUser({ notifications: false });
      setNotifications(user.token, user.id, false)
        .then(res => res.json())
        .then((result) => {
          if (!(result.success || (result.error === 'notifications_already_deactivated'))) {
            this.props.setUser({ notifications: true });
          }
        });
    } else {
      this.props.setUser({ notifications: true });
      setNotifications(user.token, user.id, true)
        .then(res => res.json())
        .then((result) => {
          if (!(result.success || (result.error === 'notifications_already_activated'))) {
            this.props.setUser({ notifications: false });
          }
        });
    }
  }

  render() {
    const {
      notifications,
    } = this.props.user;

    return (
      <SettingsContainer>
        <SimpleHeader {...this.props} title="Configurações" />
        <MenuContainer>
          <ProfileMenuItem
            key={items[0].title}
            title={items[0].title}
            switchEnabled={!notifications}
            showSwitch
            onClick={() => this.handleNotifications()}
          />
        </MenuContainer>
      </SettingsContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setUser(user) {
    dispatch(setUser(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
