import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import EventCard from '../../molecules/EventCard';
import PageLoader from '../../molecules/PageLoader';
import ScheduleHeader from '../../organisms/ScheduleHeader';
import Toaster from '../../organisms/Toaster';
import ScheduleFilter from '../../organisms/ScheduleFilter';
import { getEvents, getEventsFilterOptions } from '../../services/events';
import { addEvent, getMyEvents } from '../../services/myEvents';
import { setEvents, clearEvents } from '../../actions/events';
import { setMyEvents } from '../../actions/myEvents';

import {
  ScheduleContainer, EventList, EventsNotFound, EventsNotFoundText, EventsContainer, LoadContainer,
} from './styles';
import { categories, dates } from './data.json';

class Schedule extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      currentCategory: 'palestra',
      currentDate: '2018-09-06',
      toaster: false,
      toasterEvent: {},
      toasterLoading: false,
      toasterAdded: false,
      refreshing: false,
      filterOptions: {},
      filterOpened: false,
      filterCurrent: {},
      filterLoading: false,
      filterResult: [],
      filterNotFound: false,
    };
  }

  componentWillMount() {
    const { events } = this.props;
    const { currentDate, currentCategory } = this.state;
    if (!events[currentCategory + currentDate]) {
      this.setState({ loading: true });
      this.updateEvents(currentDate, currentCategory);
    } else {
      this.updateEvents(currentDate, currentCategory, true);
    }
    getEventsFilterOptions()
      .then(res => res.json())
      .then(result => this.setState({ filterOptions: result.result }));
  }

  handleChangeCategory(category) {
    const { events } = this.props;
    const { currentDate } = this.state;
    this.setState({
      currentCategory: category,
      filterCurrent: {},
      filterResult: [],
      filterNotFound: false,
    });
    if (!events[category + currentDate]) {
      this.setState({ loading: true });
      this.updateEvents(currentDate, category);
    } else if (!events[category + currentDate][0]) {
      this.updateEvents(currentDate, category);
    } else {
      this.updateEvents(currentDate, category, true);
    }
  }

  handleChangeDate(date) {
    const { events } = this.props;
    const { currentCategory } = this.state;
    this.setState({
      currentDate: date,
      filterCurrent: {},
      filterResult: [],
      filterNotFound: false,
    });
    if (!events[currentCategory + date]) {
      this.setState({ loading: true });
      this.updateEvents(date, currentCategory);
    } else if (!events[currentCategory + date][0]) {
      this.updateEvents(date, currentCategory);
    } else {
      this.updateEvents(date, currentCategory, true);
    }
  }

  updateEvents(currentDate, currentCategory, withoutLoad) {
    const { user } = this.props;
    if (!withoutLoad) {
      this.setState({ refreshing: true });
    }
    getEvents(user.token, user.id, currentDate, [currentCategory])
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setEvents({ [currentCategory + currentDate]: result.result });
        }
        this.setState({ refreshing: false, loading: false });
      });
  }

  updateMyEvents(currentDate) {
    const { user } = this.props;
    getMyEvents(user.token, user.id, currentDate)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setMyEvents({ [currentDate]: result.result });
        }
      });
  }

  handleAddEvent(toasterEvent) {
    const { user } = this.props;
    const { currentDate } = this.state;
    this.setState({ toaster: true, toasterEvent, toasterLoading: true });
    addEvent(user.token, user.id, toasterEvent.eventID)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.updateMyEvents(currentDate);
          this.setState({ toasterLoading: false, toasterAdded: true });
        } else if (result.error === 'event_already_added') {
          this.updateMyEvents(currentDate);
          this.setState({ toasterLoading: false, toasterAdded: true });
        }
      });
  }

  handleFilter(time, local, tags) {
    const { currentCategory, currentDate } = this.state;
    this.setState({ filterLoading: true, filterCurrent: { time, local, tags } });
    getEvents(currentDate, [currentCategory], time, local, tags)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          if (result.result[0]) {
            this.setState({
              filterLoading: false,
              filterOpened: false,
              filterResult: result.result,
              filterNotFound: false,
            });
          } else {
            this.setState({
              filterLoading: false,
              filterOpened: false,
              filterResult: result.result,
              filterNotFound: true,
            });
          }
        }
        this.setState({
          filterLoading: false,
          filterOpened: false,
        });
      });
  }

  renderItem({ item }) {
    return (
      <EventCard
        key={item.eventID}
        id={item.eventID}
        title={item.eventTitle}
        dateStart={item.eventDateStart}
        dateEnd={item.eventDateEnd}
        local={item.eventLocal}
        speaker={item.eventSpeaker}
        isFavorite={item.isFavorite}
        onEventAdded={() => this.handleAddEvent(item)}
        navigation={this.props.navigation}
      />
    );
  }

  render() {
    const {
      currentCategory,
      currentDate,
      toaster,
      toasterEvent,
      toasterLoading,
      toasterAdded,
      refreshing,
      filterOptions,
      filterOpened,
      filterLoading,
      filterCurrent,
      filterResult,
      filterNotFound,
      loading,
    } = this.state;

    const {
      events,
    } = this.props;

    const currentEvents = events[currentCategory + currentDate];

    const renderEvents = () => {
      if (loading) {
        return <LoadContainer><PageLoader /></LoadContainer>;
      }
      if (currentEvents !== undefined) {
        const currentEventsEmpty = currentEvents.length > 0;
        return (
          <EventsContainer>
            {filterResult[0] && (
            <EventList
              data={filterResult}
              keyExtractor={event => event.eventID.toString()}
              refreshing={refreshing}
              onRefresh={() => this.updateEvents(
                currentDate,
                currentCategory,
                filterCurrent.time,
                filterCurrent.local,
                filterCurrent.tags,
              )}
              renderItem={item => this.renderItem(item)}
            />
            )}
            {(currentEvents && !filterResult[0])
          && (
            <EventList
              data={currentEvents}
              keyExtractor={event => event.eventID.toString()}
              refreshing={refreshing}
              onRefresh={() => this.updateEvents(currentDate, currentCategory)}
              renderItem={item => this.renderItem(item)}
            />
          )
        }
            {((!currentEventsEmpty && !refreshing) || filterNotFound) && (
            <EventsNotFound>
              <EventsNotFoundText>
                {(currentCategory === 'palestra') ? 'Nenhuma palestra encontrada' : null}
                {(currentCategory === 'showcase') ? 'Nenhum showcase encontrado' : null}
                {(currentCategory === 'workshop') ? 'Nenhum workshop encontrado' : null}
              </EventsNotFoundText>
            </EventsNotFound>
            )}
          </EventsContainer>
        );
      }
      return null;
    };

    return (
      <ScheduleContainer>
        {toaster && (
          <Toaster
            hideToaster={() => this.setState({
              toaster: false,
              toasterLoading: false,
              toasterAdded: false,
            })}
            component={(
              <EventCard
                key={toasterEvent.eventID}
                id={toasterEvent.eventID}
                title={toasterEvent.eventTitle}
                dateStart={toasterEvent.eventDateStart}
                dateEnd={toasterEvent.eventDateEnd}
                local={toasterEvent.eventLocal}
                speaker={toasterEvent.eventSpeaker}
                navigation={this.props.navigation}
                loading={toasterLoading}
                loaded={toasterAdded}
                isFavorite
              />
            )}
          />
        )}
        <ScheduleHeader
          categories={categories}
          currentCategory={currentCategory}
          onChangeCategory={category => this.handleChangeCategory(category)}
          dates={dates}
          currentDate={currentDate}
          onChangeDate={date => this.handleChangeDate(date)}
          onOpenFilters={() => this.setState({ filterOpened: true })}
        />
        {loading && (<PageLoader />)}
        {!loading && renderEvents()}
        <ScheduleFilter
          filterOptions={filterOptions}
          opened={filterOpened}
          onFilter={(time, local, tags) => this.handleFilter(time, local, tags)}
          onCloseFilter={() => this.setState({ filterOpened: false })}
          filterLoading={filterLoading}
        />
      </ScheduleContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  events: state.events,
});

const mapDispatchToProps = dispatch => ({
  setEvents(events) {
    dispatch(setEvents(events));
  },
  setMyEvents(myEvents) {
    dispatch(setMyEvents(myEvents));
  },
  clearEvents() {
    dispatch(clearEvents());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
