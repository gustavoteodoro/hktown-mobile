import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white, darkGrey } from '../../bosons/colors';
import { RobotoRegular } from '../../bosons/fonts';

export const ScheduleContainer = styled.View`
  background-color: ${white};
  width: ${width(100)};
  height: ${height(100)};
`;

export const EventList = styled.FlatList`
  margin-top: ${width(53.25)};
`;

export const EventsNotFound = styled.View`
  width: ${width(100)};
  justify-content: center;
  align-items: center;
  padding-top: ${width(50)};
  padding-bottom: ${width(17.4)};
`;

export const EventsNotFoundText = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(4.831)};
  color: ${darkGrey};
  text-align: center;
`;

export const EventsContainer = styled.View`
  margin-bottom: ${width(17.4)};
`;

export const LoadContainer = styled.View`
  height: ${height(80)};
  position: absolute;
  top: ${height(20)};
  left: 0;
`;
