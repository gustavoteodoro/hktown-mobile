import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular, RobotoMedium, RobotoBold } from '../../bosons/fonts';
import {
  darkGrey, white, pink, lightGrey, softBlue,
} from '../../bosons/colors';

export const EventLoaderContainer = styled.View`
  flex: 1;
  background-color: ${white};
`;

export const EventGeneralContainer = styled.View`
  flex: 1;
  flex-direction: column;
  background-color: ${white};
`;

export const EventContainer = styled.ScrollView`
  flex: 1;
  flex-direction: column;
  background-color: ${white};
`;

export const EventTopImage = styled.Image`
  height: 276;
`;

export const EventContent = styled.View`
  margin-left: ${width(7.7)};
  padding-bottom: ${width(15)};
  width: ${width(84.6)};
`;

export const ShareTouch = styled.TouchableWithoutFeedback`

`;

export const ShareImage = styled.Image`
  position: absolute;
  right: 0;
  top: -${width(5.8)};
  height: ${width(11.6)};
  
`;

export const EventTitle = styled.View`
  flex-direction: row;
  align-items: flex-end;
  margin-top: 30;
`;

export const EventName = styled.Text`
  font-family: ${RobotoBold};
  color: ${pink};
  font-size: ${width(5.8)};
`;

export const EventDateContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 15;
`;

export const EventDate = styled.View`
  flex-direction: column;
  margin-left: 20;
  padding-right: 25;
  border-right-color: ${darkGrey};
  border-right-width: 2;
`;

export const EventDateText = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.865)};
  letter-spacing: 0.2;
`;

export const EventTime = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.865)};
  padding-left: 25;
`;

export const LocalContainer = styled.View`
  flex-direction: row;
  align-items: flex-start;
  margin-top: 30;
`;

export const LocalText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(3.865)};
  color: ${darkGrey};
  padding-top: 4;
  margin-left: 20;
  letter-spacing: 0.2;
`;

export const SpeakerName = styled.Text`
  font-family: ${RobotoBold};
  color: ${darkGrey};
  font-size: ${width(4.35)};
  margin-top: 20;
`;

export const SpeakerBio = styled.Text`
  font-family: ${RobotoRegular};
  color: ${darkGrey};
  font-size: ${width(3.865)};
  line-height: ${width(4.831)};
  margin-top: 10;
`;

// export const SpeakerReadMoreTouch = styled.TouchableWithoutFeedback`
// `;

export const SpeakerReadMoreContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 20;
  margin-bottom: 40;
`;

export const SpeakerReadMore = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.865)};
  color: ${softBlue};
  padding-right: 20;
`;

export const EventBio = styled.Text`
  font-family: ${RobotoRegular};
  color: ${darkGrey};
  font-size: ${width(3.865)};
  line-height: ${width(4.831)};
  margin-top: 20;
`;

export const EventSpeakersContainer = styled.View`
  margin-bottom: 30;
  width: ${width(92.6)};
`;

export const EventSpeakersTouch = styled.TouchableWithoutFeedback`
`;

export const EventEventsContainer = styled.View`
  margin-left: ${width(7.7)};
`;

export const EventSpeakersTitleContainer = styled.View`
  border-bottom-color: #c6c6c6;
  border-bottom-width: 1;
  margin-left: 30;
  padding-bottom: 20;
  padding-top: 30;
  margin-bottom: 30;
`;

export const EventSpeakersTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  color: ${pink};
`;
