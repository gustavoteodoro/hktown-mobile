import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/pt-br';
import { width } from 'react-native-dimension';
import ProfilePic from '../../atoms/ProfilePic';
import ScrollHeader from '../../molecules/ScrollHeader';
import PageLoader from '../../molecules/PageLoader';
import AddEvent from '../../atoms/AddEvent';
import { HackIcons } from '../../bosons/fonts';
import { addEvent, getMyEvents } from '../../services/myEvents';
import { getEventById } from '../../services/events';
import { setMyEvents } from '../../actions/myEvents';
import {
  EventLoaderContainer,
  EventGeneralContainer,
  EventContainer,
  EventContent,
  EventTitle,
  EventBio,
  EventName,
  EventTopImage,
  EventSpeakersTouch,
  EventEventsContainer,
  EventSpeakersTitleContainer,
  EventSpeakersTitle,
  SpeakerName,
  SpeakerBio,
  EventDate,
  EventDateContainer,
  EventTime,
  EventDateText,
  LocalContainer,
  LocalText,
  EventSpeakersContainer,
  SpeakerReadMoreContainer,
  SpeakerReadMore,
} from './styles';
import { white, pink, softBlue } from '../../bosons/colors';

moment.locale('pt-br');
class Event extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      headerMinified: false,
      event: {},
      loading: false,
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount() {
    this.setState({ loading: true });
    const { user } = this.props;
    const {
      eventId,
    } = this.props.navigation.state.params;
    getEventById(user.token, user.id, eventId)
      .then(res => res.json())
      .then((result) => {
        this.setState({ event: result.result, loading: false });
      });
  }

  handleScroll(e) {
    const { headerMinified } = this.state;
    const { y } = e.nativeEvent.contentOffset;
    if ((y > 100) && !headerMinified) {
      this.setState({
        headerMinified: true,
      });
    } else if ((y <= 100) && headerMinified) {
      this.setState({
        headerMinified: false,
      });
    }
  }

  handleAddEvent() {
    const { user } = this.props;
    const { event } = this.state;
    this.setState({ addEventLoading: true });
    const currentDay = moment(event.eventDateEnd).format('YYYY-MM-DD');
    addEvent(user.token, user.id, event.eventID)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.updateMyEvents(currentDay);
          this.setState({ addEventLoading: false, eventAdded: true });
        } else if (result.error === 'event_already_added') {
          this.updateMyEvents(currentDay);
          this.setState({ addEventLoading: false, eventAdded: true });
        }
      });
  }

  updateMyEvents(currentDate) {
    const { user } = this.props;
    getMyEvents(user.token, user.id, currentDate)
      .then(res => res.json())
      .then((result) => {
        if (result.success) {
          this.props.setMyEvents({ [currentDate]: result.result });
        }
      });
  }

  render() {
    const {
      event,
      loading,
      headerMinified,
      addEventLoading,
      eventAdded,
    } = this.state;

    return (
      <EventLoaderContainer>
        {loading && (<PageLoader />)}
        {!loading && (
        <EventGeneralContainer>
          <ScrollHeader
            {...this.props}
            color={white}
            minified={headerMinified}
            title={event.eventTitle.length > 30 ? `${event.eventTitle.slice(0, 30)}...` : event.eventTitle}
          />
          <EventContainer
            scrollEventThrottle={16}
            onScroll={this.handleScroll}
          >
            {event.eventImage && (<EventTopImage source={{ uri: event.eventImage }} />)}
            {event.eventTitle
              && (
                <EventContent>
                  <EventTitle>
                    <EventName>{event.eventTitle}</EventName>
                  </EventTitle>
                  <EventDateContainer>
                    <HackIcons name="calendar" size={width(6.52)} color={pink} />
                    <EventDate>
                      <EventDateText>{moment(event.eventDateStart).format('DD/MM')}</EventDateText>
                      <EventDateText>{moment(event.eventDateStart).format('dddd').toUpperCase()}</EventDateText>
                    </EventDate>
                    <EventTime>
                      {moment(event.eventDateStart).format('HH:mm')} — {moment(event.eventDateEnd).format('HH:mm')}
                    </EventTime>
                  </EventDateContainer>
                  <LocalContainer>
                    <HackIcons name="pin" size={width(6.52)} color={pink} />
                    <LocalText>{event.eventLocal.toUpperCase()}</LocalText>
                  </LocalContainer>
                  <EventBio>{event.eventBio.replace(/<\/?[^>]+(>|$)/g, '')}</EventBio>
                </EventContent>
              )
            }
            {event.eventSpeakers[0]
              && (
              <EventSpeakersContainer>
                <EventSpeakersTitleContainer>
                  <EventSpeakersTitle>{event.eventSpeakers[1] ? 'Palestrantes' : 'Palestrante'}</EventSpeakersTitle>
                </EventSpeakersTitleContainer>
                {event.eventSpeakers.map(speaker => (
                  <EventSpeakersTouch
                    onPress={() => this.props.navigation.navigate('Speaker', { speakerId: speaker.speakerID })}
                    key={speaker.speakerID}
                  >
                    <EventEventsContainer>
                      <ProfilePic
                        uri={speaker.speakerImage}
                        medium
                      />
                      <SpeakerName>{speaker.speakerName}</SpeakerName>
                      <SpeakerBio>{speaker.speakerBio.replace(/<\/?[^>]+(>|$)/g, '')}</SpeakerBio>
                      {/* <SpeakerReadMoreTouch> */}
                      <SpeakerReadMoreContainer>
                        <SpeakerReadMore>LER MAIS</SpeakerReadMore>
                        <HackIcons name="arrow" size={width(3.623)} color={softBlue} />
                      </SpeakerReadMoreContainer>
                      {/* </SpeakerReadMoreTouch> */}
                    </EventEventsContainer>
                  </EventSpeakersTouch>
                ))
                }
              </EventSpeakersContainer>
              )
            }
          </EventContainer>
          <AddEvent
            label="ADICIONAR EVENTO EM MINHA AGENDA"
            fixed
            labelAdded="EVENTO ADICIONADO A SUA AGENDA"
            onEventAdded={() => (!event.isFavorite ? this.handleAddEvent() : null)}
            loading={addEventLoading}
            added={eventAdded || event.isFavorite}
          />
        </EventGeneralContainer>
        )}
      </EventLoaderContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  setMyEvents(myEvents) {
    dispatch(setMyEvents(myEvents));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Event);
