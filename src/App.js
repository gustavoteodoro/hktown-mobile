import React, { PureComponent } from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { Font } from 'expo';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';
import MainNavigator from './navigators/MainNavigator';
import RobotoRegular from './assets/fonts/Roboto400.ttf';
import RobotoMedium from './assets/fonts/Roboto500.ttf';
import RobotoBold from './assets/fonts/Roboto700.ttf';
import HackIcon from './assets/fonts/hackicon.ttf';

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
    };
  }

  async componentWillMount() {
    await Font.loadAsync({
      'roboto-regular': RobotoRegular,
      'roboto-medium': RobotoMedium,
      'roboto-bold': RobotoBold,
      'hack-icons': HackIcon,
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const {
      fontLoaded,
    } = this.state;

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <StatusBar
            barStyle="dark-content"
          />
          {fontLoaded ? (
            <MainNavigator />
          ) : null}
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
