import React, { PureComponent } from 'react';
import MenuDate from '../../molecules/MenuDate';
import {
  MyCalendarHeaderContainer,
} from './styles';

class MyCalendarHeader extends PureComponent {
  render() {
    const {
      dates,
    } = this.props;
    return (
      <MyCalendarHeaderContainer>
        {dates[0]
          && <MenuDate {...this.props} large />
        }
      </MyCalendarHeaderContainer>
    );
  }
}

export default MyCalendarHeader;
