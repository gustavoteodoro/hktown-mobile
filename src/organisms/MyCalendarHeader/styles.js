import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { white } from '../../bosons/colors';

export const MyCalendarHeaderContainer = styled.View`
  width: ${width(100)};
  position: absolute;
  top:0;
  left:0;
  background: ${white};
  z-index: 20;
`;
