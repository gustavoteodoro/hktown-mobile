import React, { PureComponent } from 'react';
import {
  ToasterContainer, ToasterTouch, ToasterTouchView, ToasterContent,
} from './styles';

class Toaster extends PureComponent {
  render() {
    const {
      hideToaster,
      component,
    } = this.props;
    return (
      <ToasterContainer>
        <ToasterTouch onPress={hideToaster}>
          <ToasterTouchView />
        </ToasterTouch>
        <ToasterContent>
          {component}
        </ToasterContent>
      </ToasterContainer>
    );
  }
}

export default Toaster;
