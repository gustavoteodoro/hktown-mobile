import styled from 'styled-components';
import { width, height } from 'react-native-dimension';
import { white } from '../../bosons/colors';

export const ToasterContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  width: ${width(100)};
  height: ${height(100)};
  background-color: rgba(102, 102, 102, 0.77);
  z-index: 100;
`;

export const ToasterTouch = styled.TouchableWithoutFeedback`
`;

export const ToasterTouchView = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  width: ${width(100)};
  height: ${height(100)};
  z-index: 110;
`;

export const ToasterContent = styled.View`
  position: absolute;
  bottom: ${height(8.85)};
  left: 0;
  width: ${width(100)};
  background-color: ${white};
  z-index: 120;
`;
