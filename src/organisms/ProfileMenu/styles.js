import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { white } from '../../bosons/colors';

export const ProfileMenuContainer = styled.View`
  flex: 1;
  background-color: ${white};
  margin-top: 30;
`;

export const MenuContainer = styled.View`
  flex: 1;
  width: ${width(92.8)};
  margin-left: ${width(7.2)};
`;
