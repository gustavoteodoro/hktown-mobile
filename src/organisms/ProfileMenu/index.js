import React, { PureComponent } from 'react';
import { ProfileMenuContainer, MenuContainer } from './styles';
import ProfileMenuItem from '../../atoms/ProfileMenuItem';

import { items } from './data.json';

class ProfileMenu extends PureComponent {
  render() {
    const {
      navigation,
    } = this.props;
    return (
      <ProfileMenuContainer>
        <MenuContainer>
          {items.map(item => (
            <ProfileMenuItem
              key={item.title}
              title={item.title}
              description={item.description}
              withIcon={item.withIcon}
              onClick={() => navigation.navigate(item.link)}
            />
          ))}
        </MenuContainer>
      </ProfileMenuContainer>
    );
  }
}

export default ProfileMenu;
