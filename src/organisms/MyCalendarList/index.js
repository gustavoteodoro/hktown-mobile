import React, { PureComponent } from 'react';
import MyCalendarItem from '../../molecules/MyCalendarItem';
import {
  MyCalendarListContainer,
} from './styles';

class MyCalendarList extends PureComponent {
  render() {
    const {
      times,
    } = this.props;
    return (
      <MyCalendarListContainer>
        {times.map(time => (
          <MyCalendarItem key={time.initTime} initTime={time.initTime} endTime={time.endTime} />
        ))
        }
      </MyCalendarListContainer>
    );
  }
}

export default MyCalendarList;
