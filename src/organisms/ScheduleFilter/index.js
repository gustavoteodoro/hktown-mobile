import React, { PureComponent } from 'react';
import { width } from 'react-native-dimension';
import Button from '../../atoms/Button';
import {
  ScheduleFilterContainer,
  ScheduleFilterScroll,
  ScheduleFilterContent,
  ScheduleFilterTitle,
  ScheduleFilterCloseTouch,
  ScheduleFilterClose,
  ScheduleFilterItem,
  ScheduleFilterItemTouch,
  ScheduleFilterItemHeader,
  ScheduleFilterItemTitle,
  ScheduleFilterItemAmout,
  ScheduleFilterItemArrow,
  ScheduleFilterItemContent,
  ScheduleFilterItemOptionTouch,
  ScheduleFilterItemOption,
  ScheduleFilterItemOptionText,
  ScheduleFilterItemOptionBullet,
  ScheduleFilterButton,
} from './styles';
import { HackIcons } from '../../bosons/fonts';

import { timeOptions } from './data.json';

class ScheduleFilter extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      openedItem: '',
      selectedTime: [],
      selectedLocals: [],
      selectedTags: [],
    };
  }

  handleSelectTime(option) {
    const {
      selectedTime,
    } = this.state;

    const index = selectedTime.indexOf(option);

    if (index > -1) {
      const newSelected = selectedTime.filter(f => f !== option);
      this.setState({ selectedTime: newSelected });
    } else {
      this.setState({ selectedTime: [...selectedTime, option] });
    }
  }

  handleSelectLoal(option) {
    const {
      selectedLocals,
    } = this.state;

    const index = selectedLocals.indexOf(option);

    if (index > -1) {
      const newSelected = selectedLocals.filter(f => f !== option);
      this.setState({ selectedLocals: newSelected });
    } else {
      this.setState({ selectedLocals: [...selectedLocals, option] });
    }
  }

  handleSelectTags(option) {
    const {
      selectedTags,
    } = this.state;

    const index = selectedTags.indexOf(option);

    if (index > -1) {
      const newSelected = selectedTags.filter(f => f !== option);
      this.setState({ selectedTags: newSelected });
    } else {
      this.setState({ selectedTags: [...selectedTags, option] });
    }
  }

  handleOpenItem(item) {
    const {
      openedItem,
    } = this.state;

    if (openedItem !== item) {
      this.setState({ openedItem: item });
    } else {
      this.setState({ openedItem: '' });
    }
  }

  render() {
    const {
      opened,
      filterOptions,
      onCloseFilter,
      onFilter,
      filterLoading,
    } = this.props;

    const {
      openedItem,
      selectedTime,
      selectedLocals,
      selectedTags,
    } = this.state;

    return (
      <ScheduleFilterContainer opened={opened}>
        <ScheduleFilterScroll>
          <ScheduleFilterCloseTouch
            hitSlop={{
              top: 20, left: 20, bottom: 20, right: 20,
            }}
            onPress={() => onCloseFilter()}
          >
            <ScheduleFilterClose>
              <HackIcons name="close" size={width(4.831)} color="black" />
            </ScheduleFilterClose>
          </ScheduleFilterCloseTouch>
          <ScheduleFilterTitle>
            Descubra do seu jeito
          </ScheduleFilterTitle>
          {filterOptions.time && (
          <ScheduleFilterContent>
            <ScheduleFilterItem>
              <ScheduleFilterItemTouch onPress={() => this.handleOpenItem('time')}>
                <ScheduleFilterItemHeader>
                  <ScheduleFilterItemTitle>
                  Horário
                  </ScheduleFilterItemTitle>
                  <ScheduleFilterItemAmout>
                    {(selectedTime[0] && (selectedTime.length !== filterOptions.time.length)) ? `(${selectedTime.length})` : 'Todos'}
                  </ScheduleFilterItemAmout>
                  <ScheduleFilterItemArrow opened={(openedItem === 'time')}>
                    <HackIcons name="arrow" size={width(3.623)} color="black" />
                  </ScheduleFilterItemArrow>
                </ScheduleFilterItemHeader>
              </ScheduleFilterItemTouch>
              <ScheduleFilterItemContent opened={(openedItem === 'time')}>
                {filterOptions.time.map(option => (
                  <ScheduleFilterItemOptionTouch
                    onPress={() => this.handleSelectTime(option)}
                    key={option}
                  >
                    <ScheduleFilterItemOption>
                      <ScheduleFilterItemOptionText>{timeOptions[option]}</ScheduleFilterItemOptionText>
                      <ScheduleFilterItemOptionBullet selected={selectedTime.find(selected => selected === option)}>
                        <HackIcons name="check" size={width(2.9)} color="#F7F7F7" />
                      </ScheduleFilterItemOptionBullet>
                    </ScheduleFilterItemOption>
                  </ScheduleFilterItemOptionTouch>
                ))}
              </ScheduleFilterItemContent>
            </ScheduleFilterItem>
            <ScheduleFilterItem>
              <ScheduleFilterItemTouch onPress={() => this.handleOpenItem('local')}>
                <ScheduleFilterItemHeader>
                  <ScheduleFilterItemTitle>
                    Local
                  </ScheduleFilterItemTitle>
                  <ScheduleFilterItemAmout>
                    {(selectedLocals[0] && (selectedLocals.length !== filterOptions.local.length)) ? `(${selectedLocals.length})` : 'Todos'}
                  </ScheduleFilterItemAmout>
                  <ScheduleFilterItemArrow opened={(openedItem === 'local')}>
                    <HackIcons name="arrow" size={width(3.623)} color="black" />
                  </ScheduleFilterItemArrow>
                </ScheduleFilterItemHeader>
              </ScheduleFilterItemTouch>
              <ScheduleFilterItemContent opened={(openedItem === 'local')}>
                {filterOptions.local.map(option => (
                  <ScheduleFilterItemOptionTouch
                    onPress={() => this.handleSelectLoal(option.placeID)}
                    key={option.placeID}
                  >
                    <ScheduleFilterItemOption>
                      <ScheduleFilterItemOptionText>{option.placeName}</ScheduleFilterItemOptionText>
                      <ScheduleFilterItemOptionBullet selected={selectedLocals.find(selected => selected === option.placeID)}>
                        <HackIcons name="check" size={width(2.9)} color="#F7F7F7" />
                      </ScheduleFilterItemOptionBullet>
                    </ScheduleFilterItemOption>
                  </ScheduleFilterItemOptionTouch>
                ))}
              </ScheduleFilterItemContent>
            </ScheduleFilterItem>
            <ScheduleFilterItem>
              <ScheduleFilterItemTouch onPress={() => this.handleOpenItem('tags')}>
                <ScheduleFilterItemHeader>
                  <ScheduleFilterItemTitle>
                    Tags
                  </ScheduleFilterItemTitle>
                  <ScheduleFilterItemAmout>
                    {(selectedTags[0] && (selectedTags.length !== filterOptions.tags.length)) ? `(${selectedTags.length})` : 'Todas'}
                  </ScheduleFilterItemAmout>
                  <ScheduleFilterItemArrow opened={(openedItem === 'tags')}>
                    <HackIcons name="arrow" size={width(3.623)} color="black" />
                  </ScheduleFilterItemArrow>
                </ScheduleFilterItemHeader>
              </ScheduleFilterItemTouch>
              <ScheduleFilterItemContent opened={(openedItem === 'tags')}>
                {filterOptions.tags.map(option => (
                  <ScheduleFilterItemOptionTouch
                    onPress={() => this.handleSelectTags(option.id)}
                    key={option.id}
                  >
                    <ScheduleFilterItemOption>
                      <ScheduleFilterItemOptionText>{option.name}</ScheduleFilterItemOptionText>
                      <ScheduleFilterItemOptionBullet selected={selectedTags.find(selected => selected === option.id)}>
                        <HackIcons name="check" size={width(2.9)} color="#F7F7F7" />
                      </ScheduleFilterItemOptionBullet>
                    </ScheduleFilterItemOption>
                  </ScheduleFilterItemOptionTouch>
                ))}
              </ScheduleFilterItemContent>
            </ScheduleFilterItem>
          </ScheduleFilterContent>
          )}
        </ScheduleFilterScroll>
        <ScheduleFilterButton>
          <Button onClick={() => onFilter(selectedTime, selectedLocals, selectedTags)} label="APLICAR" loading={filterLoading} />
        </ScheduleFilterButton>
      </ScheduleFilterContainer>
    );
  }
}

export default ScheduleFilter;
