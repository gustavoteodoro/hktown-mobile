import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import {
  white, darkGrey, pink, lightGrey, lighestGrey, softBlue,
} from '../../bosons/colors';
import { RobotoBold, RobotoMedium, RobotoRegular } from '../../bosons/fonts';

export const ScheduleFilterContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  width: ${width(100)};
  height: ${height(100)};
  z-index: 100;
  flex: 1;
  background-color: ${white};
  opacity: 0;
  left: -${width(100)};
  ${props => props.opened && css`
    opacity: 1;
    left: 0;
  `}
`;

export const ScheduleFilterScroll = styled.ScrollView`

`;

export const ScheduleFilterContent = styled.View`
  margin-bottom: 160;
`;

export const ScheduleFilterTitle = styled.Text`
  margin-top: 94;
  margin-left: 30;
  font-family: ${RobotoBold};
  font-size: ${width(5.78)};
  color: ${darkGrey};
`;


export const ScheduleFilterClose = styled.View`
  position: absolute;
  top: 40;
  left: 30;
`;

export const ScheduleFilterCloseTouch = styled.TouchableWithoutFeedback`
`;

export const ScheduleFilterItem = styled.View`

`;

export const ScheduleFilterItemHeader = styled.View`
  margin-left: 30;
  padding-right: 60;
  border-bottom-width: 1;
  border-bottom-color: #707070;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ScheduleFilterItemTouch = styled.TouchableWithoutFeedback`

`;
export const ScheduleFilterItemTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  color: ${pink};
  margin-top: 20;
  margin-bottom: 20;
`;

export const ScheduleFilterItemAmout = styled.Text`

`;

export const ScheduleFilterItemArrow = styled.View`
  position: absolute;
  right: 30;
  transform: rotate(90deg);
  ${props => props.opened && css`
    transform: rotate(-90deg);
  `}
`;

export const ScheduleFilterItemContent = styled.View`
  display: none;
  background: ${lighestGrey};
  ${props => props.opened && css`
    display: flex;
  `}
`;

export const ScheduleFilterItemOptionTouch = styled.TouchableWithoutFeedback`

`;

export const ScheduleFilterItemOption = styled.View`
  margin-left: 30;
  border-bottom-width: 1;
  border-bottom-color: ${lightGrey};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

`;

export const ScheduleFilterItemOptionText = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(4.831)};
  color: ${darkGrey};
  margin-top: 20;
  margin-bottom: 20;
  width: ${width(82)};
`;

export const ScheduleFilterItemOptionBullet = styled.View`
  width: 24;
  height: 24;
  border-radius: 12;
  border-width: 1;
  border-color: ${darkGrey}
  margin-right: 30;
  align-items: center;
  justify-content: center;
  ${props => props.selected && css`
    background: ${softBlue};
  `}
`;

export const ScheduleFilterButton = styled.View`
  width: ${width(100)};
  position: absolute;
  left: 0;
  bottom: ${height(8.85)};
  background-color: ${white};
  box-shadow: -3px 3px 6px rgba(37,182,182,.16);
  padding-top: 20;
  padding-bottom: 20;
  align-items: center;
`;
