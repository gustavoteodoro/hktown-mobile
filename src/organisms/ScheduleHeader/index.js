import React, { PureComponent } from 'react';
import MenuCategory from '../../molecules/MenuCategory';
import MenuDate from '../../molecules/MenuDate';
import {
  ScheduleHeaderContainer,
  ScheduleHeaderFilterButton,
  ScheduleHeaderFilter,
  ScheduleHeaderFilterText,
} from './styles';

class ScheduleHeader extends PureComponent {
  render() {
    const {
      categories,
      dates,
      onOpenFilters,
    } = this.props;
    return (
      <ScheduleHeaderContainer>
        {categories[0]
          && <MenuCategory {...this.props} />
        }
        {dates[0]
          && <MenuDate {...this.props} />
        }
        <ScheduleHeaderFilterButton onPress={() => onOpenFilters()}>
          <ScheduleHeaderFilter>
            <ScheduleHeaderFilterText>FILTROS</ScheduleHeaderFilterText>
          </ScheduleHeaderFilter>
        </ScheduleHeaderFilterButton>
      </ScheduleHeaderContainer>
    );
  }
}

export default ScheduleHeader;
