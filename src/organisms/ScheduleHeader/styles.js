import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import { white, oneMoreLightGrey, darkGrey } from '../../bosons/colors';

export const ScheduleHeaderContainer = styled.View`
  width: ${width(100)};
  position: absolute;
  top:0;
  left:0;
  background: ${white};
  z-index: 20;
`;

export const ScheduleHeaderFilterButton = styled.TouchableWithoutFeedback`
`;

export const ScheduleHeaderFilter = styled.View`
  background: ${oneMoreLightGrey};
  justify-content: center;
  align-items: center;
  padding-bottom: ${width(5)};
  padding-top: ${width(5)};
`;

export const ScheduleHeaderFilterText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.865)};
  color: ${darkGrey};
`;
