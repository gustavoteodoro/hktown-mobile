import { SET_USER, CLEAR_USER } from '../actions/user';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case SET_USER: {
      return Object.assign({}, state, action.user);
    }
    case CLEAR_USER: {
      return {};
    }
    default:
      return state;
  }
}
