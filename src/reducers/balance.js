import { UPDATE_BALANCE } from '../actions/balance';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_BALANCE: {
      return action.balance;
    }
    default:
      return state;
  }
}
