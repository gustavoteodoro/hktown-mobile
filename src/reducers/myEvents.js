import { UPDATE_MY_EVENTS, CLEAR_MY_EVENTS } from '../actions/myEvents';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_MY_EVENTS: {
      return Object.assign({}, state, action.events);
    }
    case CLEAR_MY_EVENTS: {
      return {};
    }
    default:
      return state;
  }
}
