import { UPDATE_HIGHLIGHTS } from '../actions/highlights';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_HIGHLIGHTS: {
      return action.highlights;
    }
    default:
      return state;
  }
}
