import { UPDATE_EVENTS, CLEAR_EVENTS } from '../actions/events';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_EVENTS: {
      return Object.assign({}, state, action.events);
    }
    case CLEAR_EVENTS: {
      return {};
    }
    default:
      return state;
  }
}
