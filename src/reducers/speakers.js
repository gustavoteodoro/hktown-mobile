import { UPDATE_SPEAKERS, CLEAR_SPEAKERS } from '../actions/speakers';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SPEAKERS: {
      return action.speakers;
    }
    case CLEAR_SPEAKERS: {
      return {};
    }
    default:
      return state;
  }
}
