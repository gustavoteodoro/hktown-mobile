import { createStackNavigator } from 'react-navigation';

import Schedule from '../../pages/Schedule';
import Event from '../../pages/Event';

export default createStackNavigator(
  {
    Schedule,
    Event,
  },
  {
    initialRouteName: 'Schedule',
    navigationOptions: {
      header: null,
    },
  },
);
