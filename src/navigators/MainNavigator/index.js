import { createSwitchNavigator } from 'react-navigation';

import AuthLoading from '../../pages/AuthLoading';
import AuthNavigator from '../AuthNavigator';
import TabNavigator from '../TabNavigator';

export default createSwitchNavigator(
  {
    AuthLoading,
    AuthNavigator,
    TabNavigator,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);
