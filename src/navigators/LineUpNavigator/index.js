import { createStackNavigator } from 'react-navigation';

import LineUp from '../../pages/LineUp';
import Speaker from '../../pages/Speaker';

export default createStackNavigator(
  {
    LineUp,
    Speaker,
  },
  {
    initialRouteName: 'LineUp',
    navigationOptions: {
      header: null,
    },
  },
);
