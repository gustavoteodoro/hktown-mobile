import { createStackNavigator } from 'react-navigation';

import Onboard from '../../pages/Onboard';
import Login from '../../pages/Login';
import Regulation from '../../pages/Regulation';
import PrivacyPolicy from '../../pages/PrivacyPolicy';

export default createStackNavigator(
  {
    Onboard,
    Login,
    Regulation,
    PrivacyPolicy,
  },
  {
    initialRouteName: 'Onboard',
    navigationOptions: {
      header: null,
    },
  },
);
