import { createStackNavigator } from 'react-navigation';

import Profile from '../../pages/Profile';
import CalendarStatus from '../../pages/CalendarStatus';
import CalendarStatusOther from '../../pages/CalendarStatusOther';
import Balance from '../../pages/Balance';
import BalanceHistory from '../../pages/BalanceHistory';
import Feedback from '../../pages/Feedback';
import Regulation from '../../pages/Regulation';
import PrivacyPolicy from '../../pages/PrivacyPolicy';
import Sponsors from '../../pages/Sponsors';
import Settings from '../../pages/Settings';

export default createStackNavigator(
  {
    Profile,
    CalendarStatus,
    CalendarStatusOther,
    Balance,
    BalanceHistory,
    Feedback,
    Regulation,
    PrivacyPolicy,
    Sponsors,
    Settings,
  },
  {
    initialRouteName: 'Profile',
    navigationOptions: {
      header: null,
    },
  },
);
