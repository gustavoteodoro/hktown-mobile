import { createStackNavigator } from 'react-navigation';

import Home from '../../pages/Home';
import SearchResult from '../../pages/SearchResult';

export default createStackNavigator(
  {
    Home,
    SearchResult,
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      header: null,
    },
  },
);
