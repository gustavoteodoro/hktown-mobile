import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoBold } from '../../bosons/fonts';

export const MenuLabel = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(2.7)};
  text-align: center;
  ${props => props.color && css`
    color: ${props.color};
  `}
`;
