import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import { width, height } from 'react-native-dimension';

import { HackIcons } from '../../bosons/fonts';

import HomeNavigator from '../HomeNavigator';
import LineUpNavigator from '../LineUpNavigator';
import EventNavigator from '../EventNavigator';
import MyCalendar from '../../pages/MyCalendar';

import ProfileNavigator from '../ProfileNavigator';

import { MenuLabel } from './styles';

const navigatorHeight = height(8.85);

export default createBottomTabNavigator(
  {
    Home: HomeNavigator,
    LineUp: LineUpNavigator,
    Schedule: EventNavigator,
    MyCalendar,
    Profile: ProfileNavigator,
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = 'home';
        } else if (routeName === 'LineUp') {
          iconName = 'lineup';
        } else if (routeName === 'Schedule') {
          iconName = 'calendar';
        } else if (routeName === 'MyCalendar') {
          iconName = 'favorites';
        } else if (routeName === 'Profile') {
          iconName = 'profile';
        }
        return <HackIcons name={iconName} size={width(6.8)} color={tintColor} />;
      },
      tabBarLabel: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let itemLabel;
        if (routeName === 'Home') {
          itemLabel = 'Início';
        } else if (routeName === 'LineUp') {
          itemLabel = 'LineUp';
        } else if (routeName === 'Schedule') {
          itemLabel = 'Programação';
        } else if (routeName === 'MyCalendar') {
          itemLabel = 'Favoritos';
        } else if (routeName === 'Profile') {
          itemLabel = 'Perfil';
        }
        return <MenuLabel color={tintColor}>{itemLabel}</MenuLabel>;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#666666',
      inactiveTintColor: '#B3B3B3',
      style: {
        height: navigatorHeight,
        backgroundColor: '#fff',
        paddingTop: 9,
        paddingBottom: 9,
      },
    },
  },
);
