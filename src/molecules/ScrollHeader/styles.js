import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoBold } from '../../bosons/fonts';
import { white, pink } from '../../bosons/colors';

export const SimpleContainer = styled.View`
  position: absolute;
  width: ${width(100)};
  top: 0;
  left: 0;
  z-index: 20;
  padding-top: 34;
  padding-left: 24;
  flex-direction: row;
  padding-bottom: 10;
  align-items: center;
  ${props => props.minified && css`
    background: ${pink};
  `}
`;

export const SimpleIcon = styled.TouchableWithoutFeedback`

`;

export const SimpleText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(3.865)};
  color: ${white};
  margin-left: 20;
  opacity: 0;
  ${props => props.minified && css`
    opacity: 1;
  `}
`;
