import React, { Component } from 'react';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { SimpleContainer, SimpleIcon, SimpleText } from './styles';
import { darkGrey } from '../../bosons/colors';

class ScrollHeader extends Component {
  render() {
    const {
      title,
      navigation,
      color,
      minified,
    } = this.props;

    return (
      <SimpleIcon onPress={() => navigation.goBack(null)}>
        <SimpleContainer minified={minified}>
          <HackIcons
            name="back"
            size={width(5.8)}
            color={color || darkGrey}
          />
          <SimpleText minified={minified}>{title}</SimpleText>
        </SimpleContainer>
      </SimpleIcon>
    );
  }
}

export default ScrollHeader;
