import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import {
  pink,
  white,
  softBlue,
} from '../../bosons/colors';

export const MenuDateContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
  background: ${softBlue};
  z-index: 20;
  ${props => props.large && css`
    padding-top: 20;
  `}
`;

export const MenuDateItemContainer = styled.View`
`;

export const MenuDateItem = styled.TouchableWithoutFeedback`
`;

export const MenuDateItemUnderline = styled.View`
  position: absolute;
  width: 150%;
  height: 6;
  bottom: 0;
  left: -25%;
  background: ${pink};
  opacity: 0;

  ${props => props.active && css`
    opacity: 1;
  `}
`;

export const MenuDateContent = styled.View`
  padding-top: ${width(3.623)};
  padding-bottom: ${width(3.623)};
`;

export const MenuDateDate = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(2.9)};
  color: ${white};
  text-align: center;
`;

export const MenuDateName = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.35)};
  letter-spacing: 1;
  color: ${white};
  text-align: center;
`;
