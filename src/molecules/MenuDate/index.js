import React, { PureComponent } from 'react';
import {
  MenuDateContainer,
  MenuDateItemContainer,
  MenuDateItem,
  MenuDateContent,
  MenuDateName,
  MenuDateDate,
  MenuDateItemUnderline,
} from './styles';

class MenuDate extends PureComponent {
  render() {
    const {
      large,
      dates,
      currentDate,
      onChangeDate,
    } = this.props;

    return (
      <MenuDateContainer large={large}>
        {dates.map(date => (
          <MenuDateItemContainer key={date.id}>
            <MenuDateItem onPress={() => onChangeDate(date.id)}>
              <MenuDateContent>
                <MenuDateDate>
                  {date.date}
                </MenuDateDate>
                <MenuDateName>
                  {date.name}
                </MenuDateName>
              </MenuDateContent>
            </MenuDateItem>
            <MenuDateItemUnderline
              active={(date.id === currentDate)}
            />
          </MenuDateItemContainer>
        ))
        }
      </MenuDateContainer>
    );
  }
}

export default MenuDate;
