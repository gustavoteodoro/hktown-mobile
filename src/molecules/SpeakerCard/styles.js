import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular, RobotoMedium, RobotoBold } from '../../bosons/fonts';
import {
  darkGrey,
  lightGrey,
  pink,
} from '../../bosons/colors';

export const SpeakerCardTouch = styled.TouchableWithoutFeedback`

`;

export const SpeakerCardContainer = styled.View`
  width: ${width(90)};
  flex-direction: column;
  margin-left: ${width(5)};
  padding-bottom: ${width(1.94)};
`;

export const SpeakerCardFirstChar = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.35)};
  color: ${pink};
  margin-top: ${width(2.42)};
  margin-bottom: ${width(2.42)};
`;

export const SpeakerCardContent = styled.View`
  flex-direction: row;
`;

export const SpeakerCardTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.831)};
  color: ${darkGrey};
  text-align: left;
`;

export const SpeakerCardRole = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.38)};
  line-height: ${width(4.59)};
  color: ${lightGrey};
  text-align: left;
  margin-top: ${width(1.45)};
  margin-bottom: ${width(2.42)};
`;

export const SpeakerImage = styled.View`
  margin-left: 0;
`;

export const SpeakerInfo = styled.View`
  margin-left: ${width(5)};
  padding-top: ${width(2.42)};
  width: ${width(70)};
`;

export const SpeakerCardTagContainer = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

export const SpeakerCardTag = styled.Text`
  border: 1px solid ${lightGrey};
  border-radius: ${width(0.97)};
  color: ${lightGrey};
  font-family: ${RobotoMedium};
  font-size: ${width(2.657)};
  margin-right: ${width(1.94)};
  margin-bottom: ${width(1.94)};
  padding-bottom: ${width(0.97)};
  padding-left: ${width(1.94)};
  padding-right: ${width(1.94)};
  padding-top: ${width(0.97)};
`;
