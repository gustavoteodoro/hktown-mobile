import React from 'react';
import ProfilePic from '../../atoms/ProfilePic';

import {
  SpeakerCardTouch,
  SpeakerCardContainer,
  SpeakerCardContent,
  SpeakerCardFirstChar,
  SpeakerCardTitle,
  SpeakerCardRole,
  SpeakerImage,
  SpeakerInfo,
  SpeakerCardTagContainer,
  SpeakerCardTag,
} from './styles';

class SpeakerCard extends React.PureComponent {
  render() {
    const {
      name,
      id,
      firstLetter,
      role,
      image,
      tags,
      navigation,
    } = this.props;

    return (
      <SpeakerCardTouch onPress={() => navigation.navigate('Speaker', { speakerId: id })}>
        <SpeakerCardContainer>
          {firstLetter
            && <SpeakerCardFirstChar>{name.slice(0, 1)}</SpeakerCardFirstChar>
          }
          <SpeakerCardContent>
            <SpeakerImage>
              <ProfilePic uri={image} />
            </SpeakerImage>
            <SpeakerInfo>
              <SpeakerCardTitle>{name}</SpeakerCardTitle>
              <SpeakerCardRole>{`${role.slice(0, 60)}${(role.length > 60) ? '...' : ''}`}</SpeakerCardRole>
              {tags[0]
                && (
                <SpeakerCardTagContainer>
                  {tags.map(tag => (
                    <SpeakerCardTag key={tag}>
                      {tag}
                    </SpeakerCardTag>
                  ))}
                </SpeakerCardTagContainer>
                )
              }
            </SpeakerInfo>
          </SpeakerCardContent>
        </SpeakerCardContainer>
      </SpeakerCardTouch>
    );
  }
}

export default SpeakerCard;
