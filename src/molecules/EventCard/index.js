import React, { PureComponent } from 'react';
import moment from 'moment';
import 'moment/locale/pt-br';
import { width } from 'react-native-dimension';
import ProfilePic from '../../atoms/ProfilePic';
import EventTime from '../../atoms/EventTime';
import AddEvent from '../../atoms/AddEvent';
import { pink, darkGrey } from '../../bosons/colors';
import { HackIcons } from '../../bosons/fonts';
import {
  EventCardTouch,
  EventContainer,
  LocalContainer,
  LocalText,
  EventTitle,
  SpeakerContainer,
  SpeakerImage,
  EventCardContainer,
  SpeakerName,
  SpeakerRole,
  SpeakerTextContainer,
  EventShortcut,
  ShortcutContainer,
  ShortcutTouch,
  ShortcutContent,
} from './styles';

moment.locale('pt-br');
class EventCard extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      removed: false,
    };
  }

  handleRemove() {
    const { onRemoveEvent } = this.props;
    if (onRemoveEvent) {
      onRemoveEvent();
      this.setState({ removed: true });
    }
  }

  renderLocal() {
    const { local } = this.props;
    return (
      <LocalContainer>
        <HackIcons name="pin" size={width(4.35)} color={pink} />
        <LocalText>{local.toUpperCase()}</LocalText>
      </LocalContainer>
    );
  }


  render() {
    const {
      title,
      id,
      dateStart,
      dateEnd,
      speaker,
      navigation,
      onEventAdded,
      loading,
      loaded,
      hideAdd,
      hideTime,
      localBottom,
      shortCutAdd,
      shortCutRemove,
      onAddEvent,
      isFavorite,
    } = this.props;

    const {
      removed,
    } = this.state;
    if (removed) {
      return null;
    }
    return (
      <EventCardTouch onPress={() => navigation.navigate('Event', { eventId: id })}>
        <EventCardContainer>
          {!hideTime && (
          <EventTime
            initHour={moment(dateStart).format('HH:mm')}
            endHour={moment(dateEnd).format('HH:mm')}
          />
          )}
          <EventShortcut>
            {shortCutAdd && (
            <ShortcutContainer>
              <ShortcutTouch
                onPress={() => onAddEvent()}
                hitSlop={{
                  top: 20, left: 20, bottom: 20, right: 20,
                }}
              >
                <ShortcutContent>
                  <HackIcons name="add" size={width(2.9)} color={darkGrey} />
                </ShortcutContent>
              </ShortcutTouch>
            </ShortcutContainer>
            )}
            {shortCutRemove && (
            <ShortcutContainer>
              <ShortcutTouch
                onPress={() => this.handleRemove()}
                hitSlop={{
                  top: 20, left: 20, bottom: 20, right: 20,
                }}
              >
                <ShortcutContent>
                  <HackIcons name="delete" size={width(2.9)} color={darkGrey} />
                </ShortcutContent>
              </ShortcutTouch>
            </ShortcutContainer>
            )}
          </EventShortcut>
          <EventContainer>
            {!localBottom && (this.renderLocal())}
            <EventTitle>{title}</EventTitle>
            {speaker
            && (
            <SpeakerContainer>
              <SpeakerImage>
                <ProfilePic
                  uri={speaker.speakerImage}
                  small
                />
              </SpeakerImage>
              <SpeakerTextContainer>
                <SpeakerName>{speaker.speakerName}</SpeakerName>
                {speaker.speakerRole && (<SpeakerRole>{speaker.speakerRole}</SpeakerRole>)}
              </SpeakerTextContainer>
            </SpeakerContainer>
            )
            }
            {localBottom && (this.renderLocal())}
          </EventContainer>
          {!hideAdd
            && (
              <AddEvent
                label="ADICIONAR EVENTO EM MINHA AGENDA"
                labelAdded="EVENTO ADICIONADO A SUA AGENDA"
                onEventAdded={() => (!isFavorite ? onEventAdded() : null)}
                loading={loading}
                added={loaded || isFavorite}
              />
            )}
        </EventCardContainer>
      </EventCardTouch>
    );
  }
}

export default EventCard;
