import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular, RobotoMedium, RobotoBold } from '../../bosons/fonts';
import {
  darkGrey, pink,
} from '../../bosons/colors';

export const EventCardTouch = styled.TouchableWithoutFeedback`
`;

export const EventCardContainer = styled.View`

`;

export const EventContainer = styled.View`
  width: 80%;
  margin-top: 20;
  margin-left: 8%;
`;

export const LocalContainer = styled.View`'
  flex-direction: row;
  align-items: center;
  margin-bottom: 20;
`;

export const LocalText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(2.9)};
  color: ${darkGrey};
  margin-left: 8;
  letter-spacing: 0.2;
`;

export const EventTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.35)};
  color: ${pink};
  margin-bottom: 20;
`;

export const SpeakerContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 20;
`;

export const SpeakerImage = styled.View`

`;

export const SpeakerTextContainer = styled.View`
  flex-direction: column;
  margin-left: 15;
`;

export const SpeakerName = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(4.35)};
  color: ${darkGrey};
`;

export const SpeakerRole = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(2.9)};
  color: ${darkGrey};
`;

export const EventShortcut = styled.View`
  position: absolute;
  right: 20;
  top: 20;
`;
export const ShortcutContainer = styled.View`

`;
export const ShortcutTouch = styled.TouchableWithoutFeedback`

`;
export const ShortcutContent = styled.View`
  width: ${width(5.8)};
  height: ${width(5.8)};
  border-color: ${darkGrey};
  border-radius: ${width(2.9)};
  border-width: 2;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
