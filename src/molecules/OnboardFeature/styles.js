import styled from 'styled-components';
import { width, height } from 'react-native-dimension';

import { RobotoRegular, RobotoBold } from '../../bosons/fonts';
import { white, pink, darkGrey } from '../../bosons/colors';

export const OnboardFeatureContainer = styled.View`
  width: ${width(82)};
  height: ${height(50.1)};
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${white};
  box-shadow: 6px 3px 6px rgba(0,0,0,.16);
`;

export const OnboardFeatureTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(7.25)};
  line-height: ${width(9.42)};
  color: ${pink};
  text-align: center;
  margin-bottom: ${width(1.69)};
  margin-top: ${width(6.52)};
`;

export const OnboardFeatureDescription = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.86)};
  color: ${darkGrey};
`;
