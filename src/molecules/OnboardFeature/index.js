import React, { PureComponent } from 'react';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { pink } from '../../bosons/colors';

import { OnboardFeatureContainer, OnboardFeatureTitle, OnboardFeatureDescription } from './styles';

class OnboardFeature extends PureComponent {
  render() {
    const {
      id,
      icon,
      name,
      description,
    } = this.props;

    return (
      <OnboardFeatureContainer id={id}>
        <HackIcons name={icon} size={width(29)} color={pink} />
        <OnboardFeatureTitle>{name}</OnboardFeatureTitle>
        <OnboardFeatureDescription>{description}</OnboardFeatureDescription>
      </OnboardFeatureContainer>
    );
  }
}

export default OnboardFeature;
