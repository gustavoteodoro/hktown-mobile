import React, { PureComponent } from 'react';
import { NavigationActions } from 'react-navigation';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import {
  SimpleContainer,
  SimpleIcon,
  SimpleContent,
  SimpleText,
} from './styles';
import { darkGrey } from '../../bosons/colors';

const backAction = NavigationActions.back({
  key: null,
});
class SimpleHeader extends PureComponent {
  render() {
    const {
      title,
      navigation,
      color,
      oneLine,
      balance,
      solid,
    } = this.props;
    return (
      <SimpleContainer oneLine={oneLine} solid={solid}>
        <SimpleIcon
          onPress={() => navigation.dispatch(backAction)}
          hitSlop={{
            top: 20, left: 20, bottom: 20, right: 20,
          }}
        >
          <HackIcons
            name="back"
            size={width(5.8)}
            color={color || darkGrey}
          />
        </SimpleIcon>
        <SimpleContent>
          <SimpleText oneLine={oneLine}>{title}</SimpleText>
          {balance && (<SimpleText>{balance}</SimpleText>)}
        </SimpleContent>
      </SimpleContainer>
    );
  }
}

export default SimpleHeader;
