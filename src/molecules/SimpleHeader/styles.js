import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoBold } from '../../bosons/fonts';
import { darkGrey, white } from '../../bosons/colors';

export const SimpleContainer = styled.View`
  position: absolute;
  width: ${width(100)};
  top: 0;
  left: 0;
  z-index: 20;
  padding-top: ${width(10.24)};
  padding-left: ${width(5.8)};
  padding-right: ${width(5.8)};
  background: ${white};
  box-shadow: 0px 3px 6px rgba(0,0,0,.16);

  ${props => props.oneLine && css`
    flex-direction: row;
    padding-bottom: ${width(2.42)};
    background: ${white};
  `}
  ${props => props.solid && css`
    box-shadow: 0 0 0 rgba(0,0,0,.0);
  `}
`;

export const SimpleIcon = styled.TouchableWithoutFeedback`
  margin-top: 150px;
`;

export const SimpleContent = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const SimpleText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(5.8)};
  color: ${darkGrey};
  margin-top: ${width(6.68)};
  margin-bottom: ${width(2.9)};

  ${props => props.oneLine && css`
    margin-top: -2;
    margin-left: ${width(4.84)};
  `}
`;
