import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium, RobotoBold } from '../../bosons/fonts';
import { pink, oneMoreLightGrey, darkGrey, white } from '../../bosons/colors';

export const MenuCategoryContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-top: 25;
  padding-left: ${width(5)};
  padding-right: ${width(5)};
`;

export const MenuCategoryItemContainer = styled.View`
  justify-content: center;
  border-width: 0;
  border-style: solid;
  border-bottom-width: 6;
  border-color: ${white};
  width: ${width(25.75)};
  height: ${width(15.5)};

  ${props => props.active && css`
    border-bottom-color: ${pink};
  `}
`;

export const MenuCategoryItem = styled.TouchableWithoutFeedback`
`;

export const MenuCategoryText = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(3.865)};
  color: ${oneMoreLightGrey};
  letter-spacing: .5;
  padding-bottom: ${width(2.174)};
  padding-top: ${width(2.174)};
  text-align: center;

  ${props => props.active && css`
    font-family: ${RobotoBold};
    color: ${darkGrey};
  `}
`;
