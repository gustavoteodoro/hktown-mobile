import React, { PureComponent } from 'react';
import {
  MenuCategoryContainer,
  MenuCategoryItemContainer,
  MenuCategoryItem,
  MenuCategoryText,
} from './styles';

class MenuCategory extends PureComponent {
  render() {
    const {
      categories,
      currentCategory,
      onChangeCategory,
    } = this.props;

    return (
      <MenuCategoryContainer>
        {categories.map(category => (
          <MenuCategoryItem key={category.id} onPress={() => onChangeCategory(category.id)}>
            <MenuCategoryItemContainer active={(category.id === currentCategory)}>
              <MenuCategoryText
                active={(category.id === currentCategory)}
              >
                {category.name}
              </MenuCategoryText>
            </MenuCategoryItemContainer>
          </MenuCategoryItem>
        ))
        }
      </MenuCategoryContainer>
    );
  }
}

export default MenuCategory;
