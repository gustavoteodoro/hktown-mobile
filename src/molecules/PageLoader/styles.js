import styled from 'styled-components';
import { width, height } from 'react-native-dimension';

export const PageLoaderContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  height: ${height(100)};
  width: ${width(100)};
`;
