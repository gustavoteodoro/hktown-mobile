import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { PageLoaderContainer } from './styles';
import { softBlue } from '../../bosons/colors';

class PageLoader extends PureComponent {
  render() {
    return (
      <PageLoaderContainer>
        <ActivityIndicator size="large" color={softBlue} />
      </PageLoaderContainer>
    );
  }
}

export default PageLoader;
