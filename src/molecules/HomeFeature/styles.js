import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { RobotoRegular, RobotoBold } from '../../bosons/fonts';
import {
  white, pink, softBlue, darkGrey,
} from '../../bosons/colors';

export const HomeFeatureContainer = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  background-color: ${pink};
  padding-left: ${width(9)};
  padding-right: ${width(9)};
  padding-bottom: ${height(10)};
  ${props => (props.id === 1) && css`
    background-color: ${softBlue};
  `}
  ${props => (props.id === 2) && css`
    background-color: ${darkGrey};
  `}
`;

export const HomeFeatureTitle = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(8.7)};
  color: ${white};
  text-align: left;
  margin-bottom: ${height(4.375)};
`;

export const HomeFeatureDescription = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(4.35)};
  line-height: ${width(6.76)};
  color: ${white};
  text-align: left;
`;
