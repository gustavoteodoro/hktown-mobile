import React, { PureComponent } from 'react';

import { HomeFeatureContainer, HomeFeatureTitle, HomeFeatureDescription } from './styles';

const regex = /(<([^>]+)>)/ig;
class HomeFeature extends PureComponent {
  render() {
    const {
      id,
      name,
      description,
    } = this.props;


    const finalDescription = description.replace(regex, '');

    return (
      <HomeFeatureContainer id={id}>
        <HomeFeatureTitle>{name.toUpperCase()}</HomeFeatureTitle>
        <HomeFeatureDescription>{(finalDescription.length > 180) ? `${finalDescription.slice(0, 180)}...` : finalDescription }</HomeFeatureDescription>
      </HomeFeatureContainer>
    );
  }
}

export default HomeFeature;
