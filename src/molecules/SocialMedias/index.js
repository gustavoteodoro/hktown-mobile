import React, { PureComponent } from 'react';
import { Linking } from 'react-native';
import {
  SocialMediasContainer,
  SocialMediaTouch,
  SocialMediaView,
  SocialMediaImage,
} from './styles';
import FacebookLogo from '../../assets/logo-facebook.png';
import InstagramLogo from '../../assets/logo-instagram.png';
import YouTubeLogo from '../../assets/logo-youtube.png';
import TwitterLogo from '../../assets/logo-twitter.png';

class SocialMedias extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      speakerSocial,
    } = this.props;
    return (
      <SocialMediasContainer>
        {speakerSocial.speakerFacebook && (
          <SocialMediaTouch onPress={() => Linking.openURL(speakerSocial.speakerFacebook)}>
            <SocialMediaView>
              <SocialMediaImage
                source={FacebookLogo}
              />
            </SocialMediaView>
          </SocialMediaTouch>
        )}
        {speakerSocial.speakerInstagram && (
        <SocialMediaTouch onPress={() => Linking.openURL(speakerSocial.speakerInstagram)}>
          <SocialMediaView>
            <SocialMediaImage
              source={InstagramLogo}
            />
          </SocialMediaView>
        </SocialMediaTouch>
        )}
        {speakerSocial.speakerYoutube && (
        <SocialMediaTouch onPress={() => Linking.openURL(speakerSocial.speakerYoutube)}>
          <SocialMediaView>
            <SocialMediaImage
              source={YouTubeLogo}
            />
          </SocialMediaView>
        </SocialMediaTouch>
        )}
        {speakerSocial.speakerTwitter && (
        <SocialMediaTouch onPress={() => Linking.openURL(speakerSocial.speakerTwitter)}>
          <SocialMediaView>
            <SocialMediaImage
              source={TwitterLogo}
            />
          </SocialMediaView>
        </SocialMediaTouch>
        )}
      </SocialMediasContainer>
    );
  }
}

export default SocialMedias;
