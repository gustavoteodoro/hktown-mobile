import styled from 'styled-components';
import { width } from 'react-native-dimension';

export const SocialMediasContainer = styled.View`
  flex: 1;
  flex-direction: row;
`;

export const SocialMediaTouch = styled.TouchableWithoutFeedback`

`;

export const SocialMediaView = styled.View`
  margin-right: ${width(7.25)};
  margin-top: 20;
  margin-bottom: 20;
`;

export const SocialMediaImage = styled.Image`
  width: ${width(8.22)};
  height: ${width(8.22)};
  border-radius: ${width(4.11)};
`;
