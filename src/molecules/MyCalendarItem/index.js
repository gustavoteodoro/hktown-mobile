import React, { PureComponent } from 'react';
import EventTime from '../../atoms/EventTime';
import AddEvent from '../../atoms/AddEvent';
import {
  MyCalendarItemContainer,
} from './styles';

class MyCalendarItem extends PureComponent {
  render() {
    const {
      initTime,
      endTime,
    } = this.props;
    return (
      <MyCalendarItemContainer>
        <EventTime initHour={initTime} endHour={endTime} />
        <AddEvent label="ADICIONAR EVENTOS" />
      </MyCalendarItemContainer>
    );
  }
}

export default MyCalendarItem;
