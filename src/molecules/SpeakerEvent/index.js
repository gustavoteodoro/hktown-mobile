import React from 'react';
import moment from 'moment';
import 'moment/locale/pt-br';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { pink } from '../../bosons/colors';
import {
  EventTouch,
  EventContainer,
  EventHeader,
  EventTitle,
  EventContent,
  EventDateContainer,
  EventDate,
  EventDateText,
  EventTime,
  LocalContainer,
  LocalText,
} from './styles';

class SpeakerCard extends React.PureComponent {
  render() {
    const {
      event,
      navigation,
    } = this.props;

    return (
      <EventTouch onPress={() => navigation.navigate('Event', { eventId: event.eventID })}>
        <EventContainer>
          <EventHeader>
            <EventTitle>
              {event.eventTitle}
            </EventTitle>
          </EventHeader>
          <EventContent>
            <EventDateContainer>
              <HackIcons name="calendar" size={width(4.831)} color={pink} />
              <EventDate>
                <EventDateText>{moment(event.eventDateStart).format('DD/MM')}</EventDateText>
                <EventDateText>{moment(event.eventDateStart).format('dddd').toUpperCase()}</EventDateText>
              </EventDate>
              <EventTime>
                {moment(event.eventDateStart).format('HH:mm')} — {moment(event.eventDateEnd).format('HH:mm')}
              </EventTime>
            </EventDateContainer>
            <LocalContainer>
              <HackIcons name="pin" size={width(4.831)} color={pink} />
              <LocalText>{event.eventLocal.toUpperCase()}</LocalText>
            </LocalContainer>
          </EventContent>
        </EventContainer>
      </EventTouch>
    );
  }
}

export default SpeakerCard;
