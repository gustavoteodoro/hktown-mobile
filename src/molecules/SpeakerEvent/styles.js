import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium, RobotoBold } from '../../bosons/fonts';
import {
  darkGrey,
} from '../../bosons/colors';

export const EventTouch = styled.TouchableWithoutFeedback`

`;

export const EventContainer = styled.View`

`;
export const EventHeader = styled.View`
  padding-top: 20;
  padding-bottom: 10;
`;
export const EventTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.84)};
  color: ${darkGrey};
`;

export const EventContent = styled.View`

`;


export const EventDateContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 15;
`;

export const EventDate = styled.View`
  flex-direction: column;
  margin-left: 20;
  padding-right: 25;
  border-right-color: ${darkGrey};
  border-right-width: 1;
`;

export const EventDateText = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.382)};
  letter-spacing: 0.2;
`;

export const EventTime = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.382)};
  padding-left: 25;
`;

export const LocalContainer = styled.View`
  flex-direction: row;
  align-items: flex-start;
  margin-top: 30;
`;

export const LocalText = styled.Text`
  font-family: ${RobotoBold};
  font-size: ${width(3.382)};
  color: ${darkGrey};
  padding-top: 4;
  margin-left: 20;
  letter-spacing: 0.2;
`;
