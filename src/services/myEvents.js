import { API_URL } from '../utils/consts';

export const addEvent = (auth_token, userID, eventID) => fetch(
  `${API_URL}/myevents/add`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      eventID,
    }),
  },
);

export const removeEvent = (auth_token, userID, eventID) => fetch(
  `${API_URL}/myevents/remove`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      eventID,
    }),
  },
);

export const getMyEvents = (auth_token, userID, date, timeRange) => fetch(
  `${API_URL}/myevents/filter`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      date,
      timeRange,
    }),
  },
);

export const getMyPastEvents = (auth_token, userID) => fetch(
  `${API_URL}/myevents/past`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
    }),
  },
);

export const getMyNextEvents = (auth_token, userID) => fetch(
  `${API_URL}/myevents/next`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
    }),
  },
);

export const getMyNextEventsOther = (auth_token, userID) => fetch(
  `${API_URL}/myevents/next/other`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
    }),
  },
);
