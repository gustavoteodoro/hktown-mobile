import { API_URL } from '../utils/consts';

export const setNotifications = (auth_token, userID, notifications) => fetch(
  `${API_URL}/notifications`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      notifications,
    }),
  },
);
