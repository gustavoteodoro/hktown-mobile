import { API_URL } from '../utils/consts';

export const postEvaluate = (auth_token, userID, eventID, value) => fetch(
  `${API_URL}/events/evaluate`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      eventID,
      value,
    }),
  },
);
