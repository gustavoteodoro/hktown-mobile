const API_URL = 'http://ht2018.nowigo.com.br/sbevent/app/eventrange.mdl?action=stats';
const serviceToken = '9e2fffab-0e82-4dff-b81a-1c836463e9c7';
const eventId = '16';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getCapacity = eventID => fetch(
  `${API_URL}&event_id=${eventId}&authToken=${serviceToken}&range_uuid=${eventID}`,
  {
    headers,
    method: 'GET',
  },
);
