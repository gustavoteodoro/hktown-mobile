import { API_URL } from '../utils/consts';

export const search = searchSentence => fetch(
  `${API_URL}/search`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      searchSentence,
    }),
  },
);
