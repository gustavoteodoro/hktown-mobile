export const authLogin = (email, password) => fetch(
  'https://app.lets.events/api/account/sign_in.json',
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      user: {
        email,
        password,
      },
    }),
  },
);
