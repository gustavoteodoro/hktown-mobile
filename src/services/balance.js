const API_URL = 'http://ht2018.nowigo.com.br/sbevent/app/person.mdl?action=fetchPersonInfo';
const serviceToken = '9e2fffab-0e82-4dff-b81a-1c836463e9c7';
const eventId = '16';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getBalance = cpf => fetch(
  `${API_URL}&eventId=${eventId}&authToken=${serviceToken}&personCpf=${cpf}`,
  {
    headers,
    method: 'GET',
  },
);
