import { API_URL } from '../utils/consts';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getEventsFilterOptions = () => fetch(
  `${API_URL}/events/filter/options`,
  {
    headers,
    method: 'GET',
  },
);

export const getEvents = (auth_token, userID, date, category, time, local, tag) => fetch(
  `${API_URL}/events/filter`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      date,
      category,
      time,
      local,
      tag,
    }),
  },
);

export const getEventsByRange = (
  auth_token,
  userID,
  date,
  timeRange,
  limit,
  hideFavorites,
) => fetch(
  `${API_URL}/events/filter`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      date,
      timeRange,
      limit,
      hideFavorites,
    }),
  },
);

export const getEventById = (auth_token, userID, eventID) => fetch(
  `${API_URL}/events/single`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'auth-token': auth_token,
    },
    method: 'POST',
    body: JSON.stringify({
      eventID,
      userID,
    }),
  },
);
