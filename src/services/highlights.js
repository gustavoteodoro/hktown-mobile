import { API_URL } from '../utils/consts';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getHighlights = () => fetch(
  `${API_URL}/highlights`,
  {
    headers,
    method: 'GET',
  },
);
