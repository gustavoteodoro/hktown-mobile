import { API_URL } from '../utils/consts';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const getAllSpeakers = () => fetch(
  `${API_URL}/speakers/all`,
  {
    headers,
    method: 'GET',
  },
);

export const getSpeakerById = speakerID => fetch(
  `${API_URL}/speakers/${speakerID}`,
  {
    headers,
    method: 'GET',
  },
);
