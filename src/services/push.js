import { API_URL } from '../utils/consts';

export const setPush = (userID, notifications, pushToken) => fetch(
  `${API_URL}/notifications`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      notifications,
      pushToken,
    }),
  },
);
