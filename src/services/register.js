import { API_URL } from '../utils/consts';

export const register = (userID, cpf, hash, pushToken) => fetch(
  `${API_URL}/register`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      cpf,
      hash,
      pushToken,
    }),
  },
);

export const token = (userID, cpf, hash, pushToken) => fetch(
  `${API_URL}/token`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      userID,
      cpf,
      hash,
      pushToken,
    }),
  },
);
