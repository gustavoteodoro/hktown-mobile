import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/selection.json';

export const HackIcons = createIconSetFromIcoMoon(icoMoonConfig, 'hack-icons');
export const RobotoRegular = 'roboto-regular';
export const RobotoMedium = 'roboto-medium';
export const RobotoBold = 'roboto-bold';
