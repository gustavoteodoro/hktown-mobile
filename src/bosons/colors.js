export const black = '#000';
export const white = '#FFF';
export const darkGrey = '#666666';
export const lightGrey = '#B3B3B3';
export const lighestGrey = '#F7F7F7';
export const oneMoreLightGrey = '#D1D1D1';
export const pink = '#EF558C';
export const softBlue = '#25B6B6';
