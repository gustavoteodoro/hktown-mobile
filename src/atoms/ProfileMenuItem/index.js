import React, { PureComponent } from 'react';
import { width, height } from 'react-native-dimension';
import Ionicons from '@expo/vector-icons/Ionicons';
import Switch from '../Switch';
import {
  MenuItemContainer, ItemTitle, ItemDescription, IconContainer, TextContainer, MenuItemTouch,
} from './styles';

class ProfileMenuItem extends PureComponent {
  render() {
    const {
      title,
      description,
      withIcon,
      onClick,
      showSwitch,
      switchEnabled,
    } = this.props;
    const hasDescription = description === undefined;
    return (
      <MenuItemTouch onPress={() => onClick()}>
        <MenuItemContainer>
          <TextContainer hasDescription={hasDescription}>
            <ItemTitle>{title}</ItemTitle>
            {(description !== '')
              && <ItemDescription>{description}</ItemDescription>
            }
          </TextContainer>
          <IconContainer>
            {(withIcon)
              && <Ionicons name="ios-arrow-forward" size={width(6)} color="black" />
            }
            {showSwitch && (<Switch checked={switchEnabled} />)}
          </IconContainer>
        </MenuItemContainer>
      </MenuItemTouch>
    );
  }
}

export default ProfileMenuItem;
