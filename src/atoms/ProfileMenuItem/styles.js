import styled, { css } from 'styled-components';
import { width, height } from 'react-native-dimension';
import { darkGrey, lightGrey } from '../../bosons/colors';
import { RobotoRegular, RobotoMedium } from '../../bosons/fonts';

export const MenuItemTouch = styled.TouchableWithoutFeedback`

`;

export const MenuItemContainer = styled.View`
  flex-direction: row;
  align-items: center;
  border-bottom-color: ${lightGrey};
  border-bottom-width: 1;
  padding-top: ${height(2.71)};
  padding-right: ${width(8.2)};
  padding-bottom: ${height(1.5)};
  padding-left: 0;
  height: ${width(19.32)};
`;

export const TextContainer = styled.View`
  flex: 1;
  flex-direction: column;
  margin-bottom: 3;
  ${props => props.hasDescription && css`
    display: flex;
    align-items: center;
    flex-direction: row;
  `}
`;

export const IconContainer = styled.View`
`;

export const ItemTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  line-height: ${width(4.831)};
  color: ${darkGrey};
`;

export const ItemDescription = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.865)};
  line-height: ${width(3.865)};
  margin-top: ${width(2.25)};
  color: ${lightGrey};
`;
