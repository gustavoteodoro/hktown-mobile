import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { ButtonTouch, ButtonContainer, ButtonLabel } from './styles';

class Button extends PureComponent {
  render() {
    const {
      label,
      onClick,
      loading,
    } = this.props;

    return (
      <ButtonTouch onPress={onClick ? () => onClick() : null}>
        <ButtonContainer>
          {!loading && (<ButtonLabel>{label}</ButtonLabel>)}
          {loading && (<ActivityIndicator size="small" color="#fff" />)}
        </ButtonContainer>
      </ButtonTouch>
    );
  }
}

export default Button;
