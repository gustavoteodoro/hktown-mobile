import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular } from '../../bosons/fonts';
import { white, softBlue } from '../../bosons/colors';

export const ButtonTouch = styled.TouchableWithoutFeedback`
`;

export const ButtonContainer = styled.View`
  display: flex;
  width: ${width(82)};
  background-color: ${softBlue};
  padding-top: ${width(2.7)};
  padding-bottom: ${width(2.7)};
  box-shadow: 0px 3px 6px rgba(37,182,182,.16);
`;

export const ButtonLabel = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.86)};
  line-height: ${width(6.18)};
  color: ${white};
  text-align: center;
`;
