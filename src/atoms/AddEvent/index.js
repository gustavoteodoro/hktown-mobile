import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { width } from 'react-native-dimension';
import { softBlue } from '../../bosons/colors';
import { HackIcons } from '../../bosons/fonts';
import {
  AddEventContainer,
  AddEventText,
  AddEventTouch,
  IconContainer,
  AddContent,
  LoadingContent,
  AddedContent,
} from './styles';

class EventCard extends PureComponent {
  render() {
    const {
      label,
      fixed,
      onEventAdded,
      labelAdded,
      loading,
      added,
    } = this.props;
    return (
      <AddEventTouch
        onPress={() => (onEventAdded ? onEventAdded() : null)}
        hitSlop={{
          top: 20, left: 20, bottom: 20, right: 20,
        }}
      >
        <AddEventContainer fixed={fixed}>
          {!(loading || added)
            && (
            <AddContent>
              <IconContainer>
                <HackIcons
                  name="add"
                  size={width(2.9)}
                  color={softBlue}
                />
              </IconContainer>
              <AddEventText>{label}</AddEventText>
            </AddContent>
            )}
          {loading
            && (
              <LoadingContent>
                <ActivityIndicator size="small" color={softBlue} />
              </LoadingContent>
            )
          }
          {(added && !loading)
            && (
              <AddedContent>
                <AddEventText>{labelAdded}</AddEventText>
              </AddedContent>
            )
          }
        </AddEventContainer>
      </AddEventTouch>
    );
  }
}

export default EventCard;
