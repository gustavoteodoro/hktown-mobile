import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import {
  softBlue, white,
} from '../../bosons/colors';

export const AddEventTouch = styled.TouchableWithoutFeedback`

`;

export const AddEventContainer = styled.View`
  width: ${width(84)};
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  height: 50;
  margin-left: ${width(8)};
  ${props => props.fixed && css`
    position: absolute;
    bottom: 0;
    background-color: ${white};
    width: ${width(100)};
    margin-left: 0;
    padding-left: ${width(8)};
  `}
`;

export const IconContainer = styled.View`
  border-color: ${softBlue};
  border-width: 2;
  border-radius: 20;
  padding-top: 3;
  padding-bottom: 3;
  padding-left: 3;
  padding-right: 3;
`;

export const AddEventText = styled.Text`
  color: ${softBlue};
  font-family: ${RobotoMedium};
  font-size: ${width(3.38)};
  margin-left: 8;
  letter-spacing: 0.2;
`;

export const AddContent = styled.View`
  flex-direction: row;
  align-items: center;
`;
export const LoadingContent = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
export const AddedContent = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;
