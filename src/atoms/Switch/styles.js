import styled, { css } from 'styled-components';

export const SwitchContainer = styled.View`
  width: 51;
  height: 31;
  background: #DDDBD7;
  border-radius: 15;
  justify-content: center;

  ${props => props.checked && css`
    background: #00D020;
  `}
`;
