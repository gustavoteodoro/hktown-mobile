import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import {
  darkGrey, lighestGrey,
} from '../../bosons/colors';

export const TimeContainer = styled.View`
  width: ${width(100)};
  background-color: ${lighestGrey};
  flex-direction: row;
  align-items: center;
  height: ${width(12.08)};
`;

export const TimeText = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.865)};
  margin-left: ${width(7)};
`;
