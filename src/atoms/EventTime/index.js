import React, { PureComponent } from 'react';
import { TimeContainer, TimeText } from './styles';

class EventTime extends PureComponent {
  render() {
    const {
      initHour,
      endHour,
    } = this.props;

    return (
      <TimeContainer>
        <TimeText>{initHour} — {endHour}</TimeText>
      </TimeContainer>
    );
  }
}

export default EventTime;
