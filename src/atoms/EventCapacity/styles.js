import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoMedium } from '../../bosons/fonts';
import {
  darkGrey, white, oneMoreLightGrey,
} from '../../bosons/colors';

export const EventCapacityContainer = styled.View`
  background-color: ${white};
  flex-direction: column;
  justify-content: center;
  border-top-width: 1;
  border-top-color: #707070;
  padding-top: 25;
  padding-bottom: 25;
  
  
`;

export const EventCapacityHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: ${width(7)};
  margin-right: ${width(7)};
`;

export const EventCapacityText = styled.Text`
  font-family: ${RobotoMedium};
  color: ${darkGrey};
  font-size: ${width(3.865)};
`;

export const EventCapacityIndicators = styled.View`
  width: ${width(68)};
  flex-direction: row;
  justify-content: space-between;
  margin-left: ${width(7)};
  margin-top: 15;
`;

export const EventCapacityItem = styled.View`
  width: 32%;
  height: 11;
  border-radius: 6;
  background: ${oneMoreLightGrey};
  ${props => ((props.current === 1) && props.filled) && css`
    background: #00D020;
  `}
  ${props => ((props.current === 2) && props.filled) && css`
    background: #FF9300;
  `}
  ${props => ((props.current === 3) && props.filled) && css`
    background: #DE0404;
  `}
`;
