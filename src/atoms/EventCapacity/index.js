import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { softBlue } from '../../bosons/colors';
import {
  EventCapacityContainer,
  EventCapacityHeader,
  EventCapacityText,
  EventCapacityIndicators,
  EventCapacityItem,
} from './styles';

import { capacity, indicatorsName, NotFound } from './data.json';

class EventCapacity extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentIndicator: 1,
    };
  }

  componentWillMount() {
    const { count, eventCapacity } = this.props;
    if (count > 0) {
      const calc = (count / eventCapacity) * 100;
      if (calc > 33 && calc <= 66) {
        this.setState({ currentIndicator: 2 });
      } else if (calc > 66) {
        this.setState({ currentIndicator: 3 });
      } else {
        this.setState({ currentIndicator: 1 });
      }
    }
  }

  render() {
    const {
      loading,
      countNotFound,
    } = this.props;

    const {
      currentIndicator,
    } = this.state;


    const indicators = [1, 2, 3];

    if (loading) {
      return (
        <EventCapacityContainer>
          <ActivityIndicator size="small" color={softBlue} />
        </EventCapacityContainer>
      );
    }

    if (countNotFound) {
      return (
        <EventCapacityContainer>
          <EventCapacityHeader>
            <EventCapacityText>{NotFound}</EventCapacityText>
          </EventCapacityHeader>
        </EventCapacityContainer>
      );
    }


    return (
      <EventCapacityContainer>
        <EventCapacityHeader>
          <EventCapacityText>{capacity}</EventCapacityText>
          <EventCapacityText>{indicatorsName[`name${currentIndicator}`]}</EventCapacityText>
        </EventCapacityHeader>
        <EventCapacityIndicators>
          {indicators.map(indicator => (
            <EventCapacityItem
              key={indicator}
              current={currentIndicator}
              filled={(indicator <= currentIndicator)}
            />))}
        </EventCapacityIndicators>
      </EventCapacityContainer>
    );
  }
}

export default EventCapacity;
