import React, { PureComponent } from 'react';
import { ProfilePicContainer, ProfilePicImage } from './styles';

class ProfilePic extends PureComponent {
  render() {
    const {
      uri,
      small,
      medium,
      big,
    } = this.props;
    return (
      <ProfilePicContainer>
        <ProfilePicImage
          source={{ uri }}
          small={small}
          medium={medium}
          big={big}
        />
      </ProfilePicContainer>
    );
  }
}

export default ProfilePic;
