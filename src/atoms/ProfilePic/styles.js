import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';

export const ProfilePicContainer = styled.View`
`;

export const ProfilePicImage = styled.Image`
  width: ${width(14.98)};
  height: ${width(14.98)};
  border-radius: ${width(7.49)};
  ${props => props.small && css`
    width: ${width(8.7)};
    height: ${width(8.7)};
    border-radius: ${width(4.35)};
  `}
  ${props => props.medium && css`
    width: ${width(19.32)};
    height: ${width(19.32)};
    border-radius: ${width(9.66)};
  `}
  ${props => props.big && css`
    width: ${width(22.46)};
    height: ${width(22.46)};
    border-radius: ${width(11.23)};
  `}
`;
