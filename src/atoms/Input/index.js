import React, { PureComponent } from 'react';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import { InputContainer, InputIcon, InputField } from './styles';

class Input extends PureComponent {
  render() {
    const {
      label,
      search,
      searchFocused,
      solid,
    } = this.props;

    return (
      <InputContainer searchFocused={searchFocused}>
        {(search && !searchFocused)
          && (
          <InputIcon>
            <HackIcons name="search" size={width(5.314)} color="#666" />
          </InputIcon>
          )
        }
        <InputField {...this.props} search={search} placeholder={label} underlineColorAndroid="transparent" solid={solid} />
      </InputContainer>
    );
  }
}

export default Input;
