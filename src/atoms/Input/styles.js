import styled, { css } from 'styled-components';
import { width } from 'react-native-dimension';
import { RobotoRegular } from '../../bosons/fonts';
import { white, darkGrey } from '../../bosons/colors';

export const InputContainer = styled.View`
  position: relative;
`;

export const InputIcon = styled.View`
  position: absolute;
  top: ${width(3.38)};
  left: ${width(3.556)};
  z-index: 10;
`;

export const InputField = styled.TextInput`
  font-family: ${RobotoRegular};
  font-size: ${width(3.62)};
  background-color: ${white};
  color: ${darkGrey};
  height: ${width(12.32)};
  padding-left: 10;
  padding-right: 10;
  box-shadow: 0px 3px 3px rgba(0,0,0,0.1);
  ${props => props.search && css`
    padding-left: ${width(15.7)};
  `}
  ${props => props.searchFocused && css`
    padding-left: ${width(7)};
    font-size: ${width(5.78)};
  `}
  ${props => props.solid && css`
    box-shadow: none;
    border-bottom-width: 1;
    border-bottom-color: ${darkGrey};
  `}
`;
