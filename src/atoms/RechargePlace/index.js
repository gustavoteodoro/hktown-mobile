import React, { PureComponent } from 'react';
import { WebBrowser } from 'expo';
import { width } from 'react-native-dimension';
import { HackIcons } from '../../bosons/fonts';
import {
  RechargePlaceContainer, RechargePlaceTouch, IconContainer, TextContainer, PlaceTitle,
  PlaceAddress,
} from './styles';
import { pink } from '../../bosons/colors';

class RechargePlace extends PureComponent {
  render() {
    const {
      title,
      address,
      link,
    } = this.props;

    return (
      <RechargePlaceTouch onPress={() => WebBrowser.openBrowserAsync(link)}>
        <RechargePlaceContainer>
          <TextContainer>
            <PlaceTitle>{title}</PlaceTitle>
            <PlaceAddress>{address}</PlaceAddress>
          </TextContainer>
          <IconContainer>
            <HackIcons name="pin" size={width(7.246)} color={pink} />
          </IconContainer>
        </RechargePlaceContainer>
      </RechargePlaceTouch>
    );
  }
}

export default RechargePlace;
