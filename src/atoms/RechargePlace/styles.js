import styled from 'styled-components';
import { width } from 'react-native-dimension';
import { darkGrey, lightGrey } from '../../bosons/colors';
import { RobotoRegular, RobotoMedium } from '../../bosons/fonts';

export const RechargePlaceTouch = styled.TouchableWithoutFeedback`

`;

export const RechargePlaceContainer = styled.View`
  flex-direction: row;
  align-items: center;
  padding-top: 10;
  padding-right: 20;
  padding-bottom: 10;
  padding-left: 0;
  border-bottom-color: ${lightGrey};
  border-bottom-width: 1;
  height: 75;
`;

export const TextContainer = styled.View`
  flex: 1;
  flex-direction: column;
  margin-bottom: 3;
`;

export const IconContainer = styled.View`
  position: absolute;
  top: 22;
  right: 30;
`;

export const PlaceTitle = styled.Text`
  font-family: ${RobotoMedium};
  font-size: ${width(4.831)};
  color: ${darkGrey};
`;

export const PlaceAddress = styled.Text`
  font-family: ${RobotoRegular};
  font-size: ${width(3.865)};
  color: ${lightGrey};
`;
