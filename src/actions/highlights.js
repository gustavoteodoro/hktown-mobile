export const UPDATE_HIGHLIGHTS = 'HIGHLIGHTS/UPDATE_HIGHLIGHTS';

export function setHighlights(highlights) {
  return { type: UPDATE_HIGHLIGHTS, highlights };
}
