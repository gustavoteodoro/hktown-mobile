export const UPDATE_EVENTS = 'EVENTS/UPDATE_EVENTS';
export const CLEAR_EVENTS = 'EVENTS/CLEAR_EVENTS';

export function setEvents(events) {
  return { type: UPDATE_EVENTS, events };
}

export function clearEvents() {
  return { type: CLEAR_EVENTS };
}
