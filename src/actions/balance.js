export const UPDATE_BALANCE = 'SPEAKERS/UPDATE_BALANCE';

export function setBalance(balance) {
  return { type: UPDATE_BALANCE, balance };
}
