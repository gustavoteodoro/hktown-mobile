export const UPDATE_MY_EVENTS = 'EVENTS/UPDATE_MY_EVENTS';
export const CLEAR_MY_EVENTS = 'EVENTS/CLEAR_MY_EVENTS';

export function setMyEvents(events) {
  return { type: UPDATE_MY_EVENTS, events };
}

export function clearMyEvents() {
  return { type: CLEAR_MY_EVENTS };
}
