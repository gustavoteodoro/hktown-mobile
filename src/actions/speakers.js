export const UPDATE_SPEAKERS = 'SPEAKERS/UPDATE_SPEAKERS';
export const CLEAR_SPEAKERS = 'SPEAKERS/CLEAR_SPEAKERS';

export function setSpeakers(speakers) {
  return { type: UPDATE_SPEAKERS, speakers };
}

export function clearSpeakers() {
  return { type: CLEAR_SPEAKERS };
}
