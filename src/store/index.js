import { createStore } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';

import user from '../reducers/user';
import balance from '../reducers/balance';
import highlights from '../reducers/highlights';
import speakers from '../reducers/speakers';
import events from '../reducers/events';
import myEvents from '../reducers/myEvents';

const config = {
  key: 'HeroApp',
  storage,
  stateReconciler: autoMergeLevel1,
};

const reducer = persistCombineReducers(config, {
  user,
  balance,
  speakers,
  highlights,
  events,
  myEvents,
});

export const store = createStore(reducer);
export const persistor = persistStore(store);
